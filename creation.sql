CREATE DATABASE IF NOT EXISTS P4 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE P4;

CREATE TABLE ROLE
(
    nomrole varchar(10),
    PRIMARY KEY (nomrole)
);

insert into ROLE(nomrole)
values ('user'),
       ('admin');

CREATE TABLE MESSAGE
(
    idmsg      integer(6) unique auto_increment,
    datemsg    datetime,
    contenumsg text,
    idut       decimal(6, 0),
    PRIMARY KEY (idmsg)
);

CREATE TABLE RECEVOIR
(
    idmsg integer(6),
    idut  integer(6),
    lu    char(1),
    PRIMARY KEY (idmsg, idut)
);

CREATE TABLE UTILISATEUR
(
    idut     integer(6) unique auto_increment,
    pseudout varchar(10) unique,
    emailut  varchar(100),
    mdput    varchar(128),
    mdpSalt  varchar(64),
    activeut char(1),
    imageut  mediumblob,
    nomrole  varchar(10),
    PRIMARY KEY (idut)
);

CREATE TABLE ETREAMI
(
    idut_1 integer(6),
    idut_2 integer(6),
    PRIMARY KEY (idut_1, idut_2)
);


CREATE TABLE PARTIE
(
    idpa       integer(6) unique auto_increment,
    debutpa    datetime,
    numetape   int,
    etatpartie text,
    idut_1     integer(6),
    idut_2     integer(6),
    score_1    int,
    score_2    int,
    PRIMARY KEY (idpa)
);

CREATE TABLE INVITATION
(
    idinv     integer(6) unique auto_increment,
    dateinv   datetime,
    etatinv   char(1),
    idut_exp  integer(6),
    idut_dest integer(6),
    msginv    text,
    PRIMARY KEY (idinv)
);



ALTER TABLE MESSAGE
    ADD FOREIGN KEY (idut) REFERENCES UTILISATEUR (idut);
ALTER TABLE RECEVOIR
    ADD FOREIGN KEY (idut) REFERENCES UTILISATEUR (idut);
ALTER TABLE RECEVOIR
    ADD FOREIGN KEY (idmsg) REFERENCES MESSAGE (idmsg);
ALTER TABLE UTILISATEUR
    ADD FOREIGN KEY (nomrole) REFERENCES ROLE (nomrole);
ALTER TABLE ETREAMI
    ADD FOREIGN KEY (idut_1) REFERENCES UTILISATEUR (idut);
ALTER TABLE ETREAMI
    ADD FOREIGN KEY (idut_2) REFERENCES UTILISATEUR (idut);
ALTER TABLE PARTIE
    ADD FOREIGN KEY (idut_1) REFERENCES UTILISATEUR (idut);
ALTER TABLE PARTIE
    ADD FOREIGN KEY (idut_2) REFERENCES UTILISATEUR (idut);
ALTER TABLE INVITATION
    ADD FOREIGN KEY (idut_exp) REFERENCES UTILISATEUR (idut);
ALTER TABLE INVITATION
    ADD FOREIGN KEY (idut_dest) REFERENCES UTILISATEUR (idut);


insert into ROLE(nomrole)
values ('user'),
       ('admin');