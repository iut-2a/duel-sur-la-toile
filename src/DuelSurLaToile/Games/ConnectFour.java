package DuelSurLaToile.Games;

import DuelSurLaToile.Games.Handlers.Dev;
import DuelSurLaToile.Games.Handlers.GridController;
import DuelSurLaToile.Games.Handlers.GridHoverHandlerIn;
import DuelSurLaToile.Games.Handlers.GridHoverHandlerOut;
import DuelSurLaToile.Models.Match;
import javafx.animation.TranslateTransition;
import javafx.beans.property.BooleanProperty;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.SVGPath;
import javafx.scene.shape.Shape;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiPredicate;

/**
 * Game model for the connectFour game
 */
public class ConnectFour {
    private static final int COLUMNS = 7;
    private static final int ROWS = 6;
    private static int CIRCLE_Diameter = 80;
    /**
     * Represents the space between every pawn
     */
    private static int GAPE = CIRCLE_Diameter / 8;
    private final List<Rectangle> rectangleList;
    /*
     * It's a rectangle with holes to make the game board
     */
    private Shape rec;
    private final Pane cursorPane;

    // represents a multi-dimensional array which contains all pawns played
    private final Pawn[][] arrayInserted;
    // will render all pawns played
    private final Pane insertedPawnPane;
    private final BooleanProperty isRedPlayer;
    private final BooleanProperty isGameRunning;
    private final static Color connectFourColor = Color.valueOf("0d47a1");

    private final boolean isUser1;

    private final Match match;


    /**
     * represents a rectangle with transparent holes
     */
    public ConnectFour(BooleanProperty isRedPlayer, BooleanProperty isGameRunning, Match match) {

        this.isRedPlayer = isRedPlayer;
        this.isGameRunning = isGameRunning;

        this.match = match;

        /**
         * the user1 is by default the red player
         */
        this.isUser1 = this.isRedPlayer.get();


        this.initializeRecShape();

        this.insertedPawnPane = new Pane();
        this.arrayInserted = new Pawn[ConnectFour.ROWS][ConnectFour.COLUMNS];
        this.cursorPane = new Pane();
        this.initializeCursorList();


        // creation of the shape
        // blank ConnectFour
        this.rectangleList = new ArrayList<>();
        this.createListR();
        this.rec.getStyleClass().add("connectFour");
    }

    public static int getColumns() {
        return ConnectFour.COLUMNS;
    }

    public static int getRows() {
        return ConnectFour.ROWS;
    }

    public Shape getShape() {
        return this.rec;
    }

    public Pane getPawnPane() {
        return this.insertedPawnPane;
    }

    /*
     * this.rec have to be initialized before
     */
    private void initializeRecShape() {
        this.rec = new Rectangle((ConnectFour.COLUMNS + 1) * ConnectFour.CIRCLE_Diameter + ConnectFour.GAPE, (ConnectFour.ROWS + 1) * ConnectFour.CIRCLE_Diameter);
        for (int row = 0; row < ConnectFour.ROWS; ++row)
            for (int col = 0; col < ConnectFour.COLUMNS; ++col) {


                Circle c = new Circle(ConnectFour.CIRCLE_Diameter);
                this.formatCircle(c);
                // position of the pawn
                c.setTranslateX(col * (ConnectFour.CIRCLE_Diameter + ConnectFour.GAPE) + ConnectFour.CIRCLE_Diameter / 4);
                c.setTranslateY(row * (ConnectFour.CIRCLE_Diameter + ConnectFour.GAPE) + ConnectFour.CIRCLE_Diameter / 4);
                // we make holes for the blank Play.ConnectFour of the game
                this.rec = Shape.subtract(rec, c);

            }
    }

    /**
     * create a list of rectangles that represents the vertical bars of the game that will be contained by the Play.ConnectFour.
     */
    private void createListR() {
        for (int col = 0; col < ConnectFour.COLUMNS; ++col) {

            Rectangle rec = new Rectangle(ConnectFour.CIRCLE_Diameter + ConnectFour.GAPE, (ConnectFour.ROWS + 1) * ConnectFour.CIRCLE_Diameter);
            rec.setFill(Color.TRANSPARENT);
            rec.setTranslateX(col * (ConnectFour.CIRCLE_Diameter + ConnectFour.GAPE) + ConnectFour.CIRCLE_Diameter / 4 - ConnectFour.GAPE / 2);

            rec.setOnMouseEntered(new GridHoverHandlerIn(this, col, this.isRedPlayer));
            rec.setOnMouseExited(new GridHoverHandlerOut(col));
            this.rectangleList.add(rec);

            rec.setOnMouseClicked(new GridController(this, this.match, col));

        }
    }

    /**
     * @param c the circle that will be formatted
     */
    private void formatCircle(Circle c) {
        c.setRadius(ConnectFour.CIRCLE_Diameter / 2);
        c.setCenterX(ConnectFour.CIRCLE_Diameter / 2);
        c.setCenterY(ConnectFour.CIRCLE_Diameter / 2);
        c.setFill(Color.WHITE);
        c.setSmooth(true);
    }

    public List<Rectangle> getListR() {
        return this.rectangleList;
    }

    /**
     * @param col represents the integer of the column where we want to insert
     * @return a boolean that indicate if the player is the one who have to play or if the game is still pending
     */
    public boolean isAllowedToPlay(int col) {
        return this.isRedPlayer.get();
        // return this.isRedPlayer.getValue();
    }

    /**
     * if the collumn is full of pawn, it won't be insered
     *
     * @param col represents the integer of the column where we want to insert
     */
    public void insertPawn(int col) {

        // find where to insert it
        // starting from the bottom
        int currentRow = -1;
        for (int row = ConnectFour.ROWS - 1; row != -1; --row)
            if (Objects.isNull(this.arrayInserted[row][col])) {
                // we found the first empty spot
                currentRow = row;
                break;
            }
        if (currentRow == -1) {
            System.out.println("failed to insert a pawn in the Play.ConnectFour");
            return;
        }

        // create the pawn according to the current player and then change the player
        boolean isRed = this.isRedPlayer.getValue();
        Pawn p = new Pawn(ConnectFour.CIRCLE_Diameter, this.isRedPlayer.get());
        this.isRedPlayer.setValue(!isRed);


        if (Dev.ConnectFourDEBUGGING)
            System.out.println("next player: " + (this.isRedPlayer.getValue() ? "RED" : "YELLOW"));


        // allow pawn to be displayed
        this.insertedPawnPane.getChildren().add(p);
        // add it to our array
        this.arrayInserted[currentRow][col] = p;

        // little animation
        p.setTranslateX((col * (CIRCLE_Diameter + ConnectFour.GAPE) + CIRCLE_Diameter / 4));
        TranslateTransition translateTransition = new TranslateTransition(Duration.millis(750), p);
        translateTransition.setToY(currentRow * (CIRCLE_Diameter + ConnectFour.GAPE) + CIRCLE_Diameter / 4);
        translateTransition.play();

    }

    /**
     * initialize all shapes (triangles) that represents the selected col
     * Store them into an array
     * These triangles are transparent by default
     */
    private void initializeCursorList() {
        Shape[] cursorList = new Shape[ConnectFour.COLUMNS];
        for (int col = 0; col < ConnectFour.COLUMNS; ++col) {
            // it's a triangle that represents where the mouse is
            SVGPath triangle = new SVGPath();
            triangle.setContent("M 20 20 L 60 20 L 40 60 z");
            triangle.setContent(String.format("M %d %d L %d %d L %d %d z",
                    ConnectFour.GAPE * 2, ConnectFour.GAPE * 2,
                    ConnectFour.GAPE * 6, ConnectFour.GAPE * 2,
                    ConnectFour.GAPE * 4, ConnectFour.GAPE * 6
            ));

            triangle.setFill(Color.TRANSPARENT);


            triangle.setTranslateX(col * (ConnectFour.CIRCLE_Diameter + ConnectFour.GAPE) + ConnectFour.CIRCLE_Diameter / 4);
            triangle.setTranslateY(-ConnectFour.CIRCLE_Diameter / 8);
            //triangle.setTranslateY(Play.ConnectFour.ROWS * (Play.ConnectFour.CIRCLE_Diameter + 20) + Play.ConnectFour.CIRCLE_Diameter / 4);       // triangles are below

            cursorList[col] = triangle;
        }
        this.cursorPane.getChildren().addAll(cursorList);
    }

    public Shape getCursor(int col) {
        return (Shape) this.cursorPane.getChildren().get(col);
    }

    /**
     * @param row integer that represents the row index
     * @param col integer that represents the collumn index
     * @return the pawn at the passed position. Nb that it's null if no pawn was placed here or if it's out of bounds
     */
    public Pawn getPawn(int row, int col) {
        if (ConnectFour.ROWS < row || row < 0 || col < 0 || ConnectFour.COLUMNS < col)
            return null;
        // still may be null
        return this.arrayInserted[row][col];
    }

    public Pane getCursorPane() {
        return this.cursorPane;
    }

    /**
     * @return boolean indicates if the game is ended
     */
    public boolean win() {

        //check if the passed position is on the Play.ConnectFour and if the pawn there exists
        BiPredicate<Integer, Integer> isOnBound = (row, col) -> !(ConnectFour.ROWS < row + 1 || row < 0 || col < 0 || ConnectFour.COLUMNS < col + 1) && Objects.nonNull(this.arrayInserted[row][col]);

        if (Dev.ConnectFourDEBUGGING) {
            int i = (int) (Math.random() * ConnectFour.ROWS);
            int j = (int) (Math.random() * ConnectFour.COLUMNS);

            System.out.println("Play.Pawn at " + i + " " + j + (isOnBound.test(i, j) ? " present" : " absent"));
            System.out.println(this.arrayInserted[i][j]);

            // System.out.println("Play.Pawn at " + -i + " " + -j + (isOnBound.test(-i,-j)? " present" : " absent"));

            System.out.println("-------------------------");
        }


        // check if the color of the pawn is the same as the previous player if it's on bound
        BiPredicate<Integer, Integer> isColored = (row, col) -> isOnBound.test(row, col) && (Objects.equals(this.arrayInserted[row][col].isRed(), !this.isRedPlayer.get()));
        boolean isFilled = true;
        for (int row = 0; row < ConnectFour.ROWS; ++row) { // iterate rows, bottom to top
            for (int col = 0; col < ConnectFour.COLUMNS; ++col) { // iterate columns, left to right
                isFilled &= isOnBound.test(row, col);
                if (isColored.test(row, col) &&
                        isColored.test(row + 1, col) &&                  // up
                        isColored.test(row + 2, col) &&
                        isColored.test(row + 3, col))
                    return this.handleWin(row, col, 1);
                else if (isColored.test(row, col) &&
                        isColored.test(row, col + 1) &&
                        isColored.test(row, col + 2) &&
                        isColored.test(row, col + 3))                    // right
                    return this.handleWin(row, col, 4);
                else if (isColored.test(row, col) &&
                        isColored.test(row + 1, col + 1) &&                      // up right
                        isColored.test(row + 2, col + 2) &&
                        isColored.test(row + 3, col + 3))
                    return this.handleWin(row, col, 2);
                else if (isColored.test(row, col) &&
                        isColored.test(row + 1, col - 1) &&                       // up left
                        isColored.test(row + 2, col - 2) &&
                        isColored.test(row + 3, col - 3))
                    return this.handleWin(row, col, 3);
            }
        }

        return isFilled; // nothing found or game full
    }

    public boolean isRunning() {
        return this.isGameRunning.get();
    }

    public void setRunning(boolean isRunning) {
        this.isGameRunning.setValue(isRunning);
        if (Dev.ConnectFourDEBUGGING)
            System.out.println("Game ended, winner if winner" + (this.isRedPlayer.get() ? "Yellow" : "Red"));
    }

    /**
     * @param row int that represents the selected row
     * @param col int that represents the selected collumn
     * @param dir int that represents the dkirection of the winning "pack"
     *            1 for up, 2 for top right,
     *            3 for top left and 4 for right
     * @return boolean that indicate if the game is finished
     */
    private boolean handleWin(int row, int col, int dir) {
        int rowShift;
        int colShift;
        switch (dir) {
            case 1:
                rowShift = 1;
                colShift = 0;
                break;
            case 2:
                rowShift = 1;
                colShift = 1;
                break;
            case 3:                                                         // up left
                rowShift = 1;
                colShift = -1;
                break;
            default:                                                        //  right
                rowShift = 0;
                colShift = 1;
        }

        for (int i = 0; i < ConnectFour.ROWS; ++i) {
            // we check if we have more than 4 pawns placed
            try {
                if (this.arrayInserted[row + i * rowShift][colShift * i + col].isRed() == this.isRedPlayer.get())        // check if this pawn is of the correct color
                    return true;
            } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
                return true;        // there's a better way but it will be good
                // todo
            }
            this.arrayInserted[row + i * rowShift][colShift * i + col].highlight();
        }

        return true;
    }

    public void reset() {
        this.isGameRunning.setValue(true);
        this.isRedPlayer.setValue(Math.random() > 0.5);
        this.insertedPawnPane.getChildren().clear();
        for (int r = 0; r < this.arrayInserted.length; ++r)
            for (int c = 0; c < this.arrayInserted[r].length; ++c)
                this.arrayInserted[r][c] = null;
        int a = 2;
    }

    /**
     * @param diameter int that represents the new diameter
     * @hidden not working
     */
    public void resize(int diameter) {
        ConnectFour.CIRCLE_Diameter = diameter;
        ConnectFour.GAPE = CIRCLE_Diameter / 8;
        this.cursorPane.getChildren().clear();
        this.initializeCursorList();
        this.createListR();
        this.initializeRecShape();

        for (Pawn[] pL : this.arrayInserted)
            for (Pawn p : pL)
                if (Objects.nonNull(p))
                    p.autosize();

    }

    public boolean isUser1() {
        return this.isUser1;
    }
}
