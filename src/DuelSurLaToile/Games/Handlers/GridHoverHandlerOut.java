package DuelSurLaToile.Games.Handlers;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

/**
 * Used when the mouse exit the grid zone
 */
public class GridHoverHandlerOut implements EventHandler<MouseEvent> {
    private final int col;

    public GridHoverHandlerOut(int col) {
        super();
        this.col = col;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        Shape rec = (Shape) mouseEvent.getSource();
        rec.setFill(Color.TRANSPARENT);
    }
}
