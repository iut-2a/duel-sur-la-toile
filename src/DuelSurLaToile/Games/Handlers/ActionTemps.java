package DuelSurLaToile.Games.Handlers;

import DuelSurLaToile.Games.Chronometre;
import javafx.animation.KeyFrame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Contrôleur du chronomètre
 */
public class ActionTemps implements EventHandler<ActionEvent> {
    /**
     * Vue du chronomètre
     */
    private final Chronometre chrono;
    /**
     * temps enregistré lors du dernier événement
     */
    private long tempsPrec;
    /**
     * temps écoulé depuis le début de la mesure
     */
    private long tempsEcoule;

    /**
     * Constructeur du contrôleur du chronomètre
     * noter que le modèle du chronomètre est tellement simple
     * qu'il est inclus dans le contrôleur
     *
     * @param chrono Vue du chronomètre
     */
    public ActionTemps(Chronometre chrono) {
        this.chrono = chrono;
    }

    /**
     * Actions à effectuer tous les pas de temps
     * essentiellement mesurer le temps écoulé depuis la dernière mesure
     * et mise à jour de la durée totale
     *
     * @param actionEvent événement Action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        this.tempsPrec = (long) ((KeyFrame) actionEvent.getSource()).getTime().toMillis();
        this.tempsEcoule += this.tempsPrec;
        this.chrono.setTime(this.tempsEcoule);


    }

    /**
     * Remet la durée à 0
     */
    public void reset() {
        this.tempsPrec = 0;
        this.tempsEcoule = 0;
    }


}
