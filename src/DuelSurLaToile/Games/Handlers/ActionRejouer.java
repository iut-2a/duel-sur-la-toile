package DuelSurLaToile.Games.Handlers;

import DuelSurLaToile.Games.ConnectFour;
import DuelSurLaToile.View.GamePane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton rejouer
 */
public class ActionRejouer implements EventHandler<ActionEvent> {
    /**
     * modèle du jeu
     */
    private final ConnectFour c;
    /*
     * vue du jeu
     **/
    private final GamePane g;

    /**
     * @param c modèle du jeu
     * @param g vue du jeu
     */
    public ActionRejouer(ConnectFour c, GamePane g) {
        this.c = c;
        this.g = g;
    }


    /**
     * L'action consiste à recommencer une partie. Il faut vérifier qu'il n'y avait pas une partie en cours
     *
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button bt = (Button) actionEvent.getSource();
        switch (bt.getText()) {
            case "replay":
                this.c.reset();
                break;
            case "surrender":
                showConfirmation();
                break;
        }
    }

    private void showConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Leaving this game");
        alert.setContentText("Are you sure you really want to surrender\nthis game?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {            // ... user chose OK
            this.c.reset();
            this.g.reset();
        } else {
            return; // nothing
        }
    }
}
