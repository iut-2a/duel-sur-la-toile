package DuelSurLaToile.Games.Handlers;

import DuelSurLaToile.Games.ConnectFour;
import javafx.beans.property.BooleanProperty;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Used when the mouse enter the grid zone
 */
public class GridHoverHandlerIn implements EventHandler<MouseEvent> {
    private final int col;
    private final ConnectFour c;
    private final BooleanProperty isRed;

    public GridHoverHandlerIn(ConnectFour connectFour, int col, BooleanProperty isRed) {
        super();
        this.c = connectFour;
        this.col = col;
        this.isRed = isRed;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        Rectangle rec = (Rectangle) mouseEvent.getSource();
        rec.setFill(Color.valueOf("#eeeeee50"));

        // colorize current cursor according to the player who is currently playing
        this.c.getCursor(this.col).setFill(this.isRed.getValue() ? Color.valueOf("dd2c00") : Color.valueOf("fdd835"));
        // hide every triangles except the last one where the mouse was
        for (int j = 0; j < ConnectFour.getColumns(); ++j)
            if (j != this.col)
                this.c.getCursor(j).setFill(Color.TRANSPARENT);

    }
}

