package DuelSurLaToile.Games.Handlers;

import DuelSurLaToile.Games.ConnectFour;
import DuelSurLaToile.Models.Match;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * When whe click on a row
 */
public class GridController implements EventHandler<MouseEvent> {
    private final int col;
    private final ConnectFour c;
    private final Match match;

    public GridController(ConnectFour connectFour, Match match, int col) {
        this.c = connectFour;
        this.col = col;
        this.match = match;
    }


    @Override
    public void handle(MouseEvent mouseEvent) {
        if (!this.c.isRunning()) {
            System.out.println("Game already ended");
            return;
        }
        if (this.c.isAllowedToPlay(this.col)) {
            this.c.insertPawn(this.col);
            this.match.updateState((this.c.isUser1() ? "+u1" : "u2") + " " + this.col);

        }
        if (this.c.win()) {
            this.c.setRunning(false);
            this.match.updateState("Finished " + col);

        }
    }


}
