package DuelSurLaToile.Games;

import DuelSurLaToile.Games.Handlers.ActionTemps;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.util.Duration;

/**
 * Permet de gérer un label associé à une Timeline pour afficher un temps écoulé
 */
public class Chronometre extends Label {
    /**
     * timeline qui va gérer le temps
     */
    private final Timeline timeline;
    /**
     * la fenêtre de temps
     */
    private final KeyFrame keyFrame;
    /**
     * le contrôleur associé au chronomètre
     */
    private final ActionTemps actionTemps;

    /**
     * Constructeur permettant de créer le chronomètre
     * avec un label initialisé à "0:0:0"
     * Ce constructeur créer la Timeline, la KeyFrame et le contrôleur
     */
    public Chronometre() {
        super("0:0:0");

        this.actionTemps = new ActionTemps(this);
        this.keyFrame = new KeyFrame(Duration.seconds(1), this.actionTemps);

        this.timeline = new Timeline(this.keyFrame);
        this.timeline.setCycleCount(Timeline.INDEFINITE);
    }

    /**
     * Permet au controleur de mettre à jour le label
     * la durée est affichée sous la forme h:m:s
     *
     * @param tempsMillisec la durée depuis à afficher
     */
    public void setTime(long tempsMillisec) {
        long second = (tempsMillisec / 1000) % 60;
        long minute = (tempsMillisec / (1000 * 60)) % 60;
        long hour = (tempsMillisec / (1000 * 60 * 60)) % 24;

        String time = String.format("%1d:%1d:%1d", hour, minute, second);
        this.setText(time);
    }

    /**
     * Permet de démarrer le chronomètre
     */
    public void start() {
        this.timeline.play();
    }

    /**
     * Permet d'arrêter le chronomètre
     */
    public void stop() {
        this.timeline.stop();
    }

    /**
     * Permet de remettre le chronomètre à 0
     */
    public void resetTime() {
        this.setTime(0);
        this.timeline.playFromStart();
        this.actionTemps.reset();
    }
}
