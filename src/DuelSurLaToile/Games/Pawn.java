package DuelSurLaToile.Games;

import javafx.animation.FillTransition;
import javafx.animation.Timeline;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;


public class Pawn extends Circle {
    private final double cRadius;
    private final boolean isRed;
    private final Color color;

    public Pawn(double radius, boolean isRed) {
        this.cRadius = radius;
        this.isRed = isRed;
        setRadius(radius / 2);
        this.color = isRed ? Color.valueOf("dd2c00") : Color.valueOf("fdd835");
        setFill(this.color);
        setCenterX(radius / 2);
        setCenterY(radius / 2);


/*
        super.setOnMouseClicked(mouseEvent -> {
            if (!this.isPlayed())
                this.setFill(this.isRed ? Color.RED : Color.YELLOW);
        });
*/
    }

    public boolean isRed() {
        return this.isRed;
    }

    @Override
    public String toString() {
        return "" + (this.isRed ? "red" : "yellow") + " pawn";
    }

    public void highlight() {
        // target color
        Color colorF = isRed ? Color.DARKRED : Color.valueOf("#bfff00");
        //Instantiating Fill Transition class
        FillTransition fill = new FillTransition();

        //The transition will set to be auto reserved by setting this to true
        fill.setAutoReverse(true);

        //setting cycle count for the fill transition
        fill.setCycleCount(Timeline.INDEFINITE);

        //setting duration for the Fill Transition
        fill.setDuration(Duration.millis(2000));

        //setting the Intital from value of the color
        fill.setFromValue(this.color);

        //setting the target value of the color
        fill.setToValue(colorF);

        //setting polygon as the shape onto which the fill transition will be applied
        fill.setShape(this);

        //playing the fill transition
        fill.play();
        //this.setFill(isRed ? Color.DARKRED: Color.valueOf("#CCCC00"));
    }
}