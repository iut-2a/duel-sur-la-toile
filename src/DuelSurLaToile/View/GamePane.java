package DuelSurLaToile.View;

import DuelSurLaToile.Database.DatabaseMatch;
import DuelSurLaToile.Games.Chronometre;
import DuelSurLaToile.Games.ConnectFour;
import DuelSurLaToile.Games.Handlers.ActionRejouer;
import DuelSurLaToile.Main;
import DuelSurLaToile.Models.Match;
import DuelSurLaToile.View.Handlers.GameStartHandler;
import DuelSurLaToile.View.Handlers.WaitForGameHandler;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.util.Duration;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

/**
 * Mainly exists because else way PlayPane was adding itself and it's a cycle
 * and to handle multiple games
 */
public class GamePane extends GridPane {
    private ConnectFour connectFour;
    /**
     * It's a shared boolean to indicate if the current user is the red player or not
     * By default, the red player is the first player of a game
     */
    private BooleanProperty isRedPlayer;
    private BooleanProperty isGameRunning;
    private Pane connectFourView;
    private PlayPane p;
    private List<Rectangle> listR;
    private final Main m;
    private Chronometre chrono;
    private Button regame;
    private Shape rec;
    private VBox buttonPane;
    private Button surrender;
    private Button btCreateGame;

    /**
     * show who's player turn and show up and ending message
     */
    private Label lbPlayerTurn;
    /**
     * The current match
     */
    private Match match;
    /**
     * Exists only when we are waiting for a player
     */
    private HBox waitingBox;

    /**
     * the number user waiting game
     */
    private int gameNumber;
    /**
     * contains an event to handle every second to know the game state
     */
    private Timeline timelineWaiting;


    /**
     * @param p
     * @param isActive if the game is activated
     */
    public GamePane(PlayPane p, boolean isActive) {
        super();
        this.m = p.getMain();
        this.p = p;
        if (isActive)
            this.attemptCreateGame();
        else
            this.createPanel();

    }

    /**
     * create a new started game
     *
     * @param p
     */
    public GamePane(PlayPane p) {
        this(p, true);
    }

    /**
     * @return true if the game was started
     */
    public boolean isActive() {
        return Objects.nonNull(this.isGameRunning);
    }


    private void createPanel() {
        this.btCreateGame = new Button("Start a new game");
        this.btCreateGame.setOnAction(new GameStartHandler(this.getPlayPane()));
        this.getChildren().add(this.btCreateGame);
    }

    /**
     * initialize the connectFour
     * if a game was found, create a new game
     */
    private void attemptCreateGame() {
        Match toStart = DatabaseMatch.getMatchWaitingP();

        if (Objects.nonNull(toStart)) {
            this.isRedPlayer = new SimpleBooleanProperty(false);
            this.match = toStart;
            toStart.addUser(this.m.getCurrentUser());
            // set an event every second to know when the opponent played
            this.handleWaitPlayer();
            this.stopWaiting(); //bad implementation // todo
            this.createGame();

        } else {
            this.isRedPlayer = new SimpleBooleanProperty(true);
            handleNoMatchFound();
        }

    }

    public void createGame() {

        this.isGameRunning = new SimpleBooleanProperty(true);
        this.lbPlayerTurn = new Label();
        // change this String value every time a player turn is over
        this.lbPlayerTurn.textProperty().bind(Bindings.createStringBinding(
                () -> {
                    if (this.isGameRunning.get())
                        return "It's " + (this.isRedPlayer.get() ? "your" : "his") + " turn";
                    else
                        return "Game ended, " + (this.connectFour.isUser1() && this.isRedPlayer.get() ? "red" : "yellow") + " winner";
                }, this.isRedPlayer, this.isGameRunning
        ));

        Label iNeedGape = new Label();
        iNeedGape.setPrefWidth(75.);
        this.add(iNeedGape, 0, 0);        // absolutely awful     // todo better

        this.connectFour = new ConnectFour(this.isRedPlayer, this.isGameRunning, this.match);
        this.connectFourView = this.connectFour();

        this.p = p;
        this.add(this.connectFourView, 1, 1);
        this.add(this.leChrono(), 1, 0);
        this.add(this.lbPlayerTurn, 1, 0);
        this.setAlignment(Pos.CENTER);
        this.add(this.initializeButtons(), 2, 1);
        this.buttonPane.setAlignment(Pos.BOTTOM_CENTER);


        System.out.println("Grid created");
    }


    private void handleNoMatchFound() {
        Match toAdd = new Match(this.m.getCurrentUser(), Match.WAITING);
        this.match = toAdd;
        try {
            DatabaseMatch.addWaiting(toAdd);
            this.match.update();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return;
        }
        handleWaitPlayer();
    }

    /**
     *
     */
    private void handleWaitPlayer() {

        KeyFrame timeEvent = new KeyFrame(Duration.seconds(5), new WaitForGameHandler(this, this.isRedPlayer.get()));
        this.timelineWaiting = new Timeline(timeEvent);
        timelineWaiting.setCycleCount(Timeline.INDEFINITE);
        timelineWaiting.play();
        this.waitingBox = new HBox();
        ProgressIndicator progress = new ProgressIndicator();
        waitingBox.getChildren().addAll(progress, new Label("Waiting for a player"));
        this.getChildren().add(waitingBox);
    }

    /**
     * ask to the db if an update's here
     */
    public boolean askUpdatePlayer() {
        Match wantMatch = DatabaseMatch.getMatchById(this.match.getId());
        if (wantMatch.equals(this.match))             // match wasn't updated
            return false;
        else if (Objects.nonNull(wantMatch.getUser2())) {  // yes second user found
            this.match = wantMatch;
            this.getChildren().remove(this.waitingBox);     // no more need
            return true;
        }
        System.out.println("ok match updated but there isn't second player");
        return false;
    }

    /**
     * @return a Pane that contains the Surrender and replay buttons
     */
    private VBox initializeButtons() {
        this.buttonPane = new VBox();
        this.buttonPane.getChildren().addAll(rejouer(), surrender());
        return this.buttonPane;
    }

    /**
     * retyrb the connectFourview
     *
     * @return
     */
    private Pane connectFour() {
        GridPane res = new GridPane();
        res.add(this.connectFour.getCursorPane(), 1, 0);
        this.rec = this.connectFour.getShape();
        res.add(this.rec, 1, 1);
        res.add(this.connectFour.getPawnPane(), 1, 1);

        for (Rectangle r : this.connectFour.getListR())
            res.add(r, 1, 1);

        res.setMinHeight(600);
        res.setAlignment(Pos.CENTER);
        return res;
    }

    private Button rejouer() {

        this.regame = new Button("replay");
        this.regame.setMinSize(100, 50);
        this.regame.setOnAction(new ActionRejouer(this.connectFour, this));
        this.regame.setDisable(true);
        return this.regame;
    }

    private Button surrender() {
        this.surrender = new Button("surrender");
        surrender.setMinSize(100, 50);
        surrender.setOnAction(new ActionRejouer(this.connectFour, this));    // todo, ActionSurrender
        surrender.setDisable(false);
        return surrender;
    }

    /**
     * @return a pane that contains game chronometer
     */
    private Node leChrono() {
        this.chrono = new Chronometre();
        this.chrono.start();
        VBox res = new VBox(5);
        Label chrono = new Label("Chrono");
        ImageView img = new ImageView();
        /*
        File file = new File("src/resources/images/chrono.png");

        Image image = new Image(file.toURI().toString());
        img.setImage(image);
        img.setFitWidth(20);
        img.setFitHeight(20);
         */         // todo

        chrono.setStyle("-fx-font: 2em Tahoma;\n" +
                "-fx-stroke: black;\n" +
                "-fx-stroke-width: 1;");
        res.getChildren().add(chrono);
        res.getChildren().add(this.chrono);
        res.setAlignment(Pos.CENTER);
        this.isGameRunning.addListener((observable, oldValue, newValue) -> {
            this.regame.setDisable(newValue);
            this.surrender.setDisable(oldValue);
            if (newValue) {
                this.chrono.resetTime();
                this.chrono.start();
            } else {
                this.chrono.stop();
            }
        });

        return res;


    }

    /**
     * used when we stop the game
     */
    public void reset() {
        // we stop the chronometer
        this.chrono.stop();
    }

    /**
     * recreate a game
     */
    public void startNewGame(int gameNumber) {
        this.getChildren().clear();
        this.gameNumber = gameNumber;
        this.attemptCreateGame();

    }

    /**
     * @return the current playPane
     */
    public PlayPane getPlayPane() {
        return this.p;
    }

    public int getGameNumber() {
        return this.gameNumber;
    }

    public Match getMatch() {
        return this.match;
    }

    public ConnectFour getConnectFourModel() {
        return this.connectFour;
    }

    public void stopWaiting() {
        this.getChildren().remove(this.waitingBox);
    }
}
