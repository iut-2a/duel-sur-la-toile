package DuelSurLaToile.View.Util;

import DuelSurLaToile.Models.ContentInterface;

/**
 * Contains all buttons when the user isn't logged
 */
public enum VisitorHeaderContent implements ContentInterface {
    Login("Login"),
    Register("Register");

    /**
     * the value to put on the button
     */
    private final String value;

    VisitorHeaderContent(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return this.value;
    }
}
