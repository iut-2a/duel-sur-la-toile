package DuelSurLaToile.View.Util;


import DuelSurLaToile.Models.ContentInterface;

/**
 * Contains all navbar buttons
 * all these values must have a get method to their name in the main
 */
public enum ProfileContent implements ContentInterface {
    Overview("Overview"),
    History("History");

    /**
     * the value to put on the button
     */
    private final String value;

    ProfileContent(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return this.value;
    }

}
