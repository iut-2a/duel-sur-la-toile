package DuelSurLaToile.View.Util;

import DuelSurLaToile.Models.User;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

import java.io.ByteArrayInputStream;
import java.util.Arrays;

/**
 * Represents the Picture of an user
 */
public class UserAvatar extends Circle {
    /**
     * default radius of image, used when we don't give height to the constructor
     */
    private static final int DEFAULTAILLE = 30;
    /**
     * contains the bytes of the picture represented
     */
    private final byte[] imgBytes;

    /**
     * @param imgBytes contains the blob contents
     */
    public UserAvatar(byte[] imgBytes) {
        this(imgBytes, DEFAULTAILLE);
    }

    public UserAvatar(byte[] imgBytes, double taille) {
        super(taille);
        System.out.println("creating user avatar from bytes");
        this.setRadius(taille);
        this.imgBytes = imgBytes;
        Image img = new Image(new ByteArrayInputStream(imgBytes));

        this.setFill(getRoungImage(img));
        this.getStyleClass().add("UserAvatar");
        // todo set on click event
    }

    /**
     * Constructor with default image
     */
    public UserAvatar() {
        // default img
        this(DEFAULTAILLE);
    }

    /**
     * Constructor with default image
     *
     * @param taille the UserAvatar radius
     */
    public UserAvatar(double taille) {
        // default img
        super(taille);
        this.imgBytes = new byte[]{1};
        this.setFill(this.getRoungImage(User.DEFAULTAVATAR));


    }

    /**
     * @param img the Image to be rounded
     * @return a new Paint with the rounded Image
     */
    private Paint getRoungImage(Image img) {
        ImagePattern pattern = new ImagePattern(img);
        return pattern;
    }

    /**
     * @return the corresponding byte[] of the img represented
     */
    public byte[] toBytes() {
        return this.imgBytes;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserAvatar)) return false;

        UserAvatar that = (UserAvatar) o;

        return Arrays.equals(imgBytes, that.imgBytes);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(imgBytes);
    }
}
