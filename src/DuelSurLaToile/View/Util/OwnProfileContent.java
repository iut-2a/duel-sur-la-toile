package DuelSurLaToile.View.Util;

import DuelSurLaToile.Models.ContentInterface;

/**
 * represents all buttons that the user can see only on his own profile
 */
public enum OwnProfileContent implements ContentInterface {
    Social("Social");

    /**
     * the value to put on the button
     */
    private final String value;

    OwnProfileContent(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return this.value;
    }
}
