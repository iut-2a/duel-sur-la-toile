package DuelSurLaToile.View.Util;


import DuelSurLaToile.Models.ContentInterface;

/**
 * Contains all navbar buttons
 * all these values must have a get method to their name in the main
 */
public enum BoardContent implements ContentInterface {
    Game("Game"),
    Social("Social");

    /**
     * the name of the tab
     */
    private final String value;

    BoardContent(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

}
