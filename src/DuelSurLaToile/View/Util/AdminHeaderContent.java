package DuelSurLaToile.View.Util;

import DuelSurLaToile.Models.ContentInterface;

/**
 * All buttons that only an admin can see in the navbar (so when you are logged as an admin)
 */
public enum AdminHeaderContent implements ContentInterface {
    Admin("Admin");

    /**
     * the value to put on the button
     */
    private final String value;

    AdminHeaderContent(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return this.value;
    }

}
