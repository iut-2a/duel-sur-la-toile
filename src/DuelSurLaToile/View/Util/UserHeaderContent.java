package DuelSurLaToile.View.Util;


import DuelSurLaToile.Models.ContentInterface;

/**
 * Contains all navbar buttons
 * all these values must have a get method to their name in the main
 */
public enum UserHeaderContent implements ContentInterface {
    Home("Home"),
    Profile("Profile"),
    Play("Play");

    /**
     * the value to put on the button
     */
    private final String value;

    UserHeaderContent(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return this.value;
    }
}
