package DuelSurLaToile.View.Util;


import DuelSurLaToile.Models.ContentInterface;

/**
 * Contains all navbar buttons
 * all these values must have a get method to their name in the main
 */
public enum HeaderContent implements ContentInterface {
    Home("Home"),
    Profile("Profile"),
    Play("Play");
    /**
     * the value to put on the buttons
     */
    private final String value;

    HeaderContent(String value) {
        this.value = value;
    }


    public String getValue() {
        return this.value;
    }
}
