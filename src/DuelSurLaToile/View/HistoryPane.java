package DuelSurLaToile.View;

import DuelSurLaToile.Database.DatabaseMatch;
import DuelSurLaToile.Main;
import DuelSurLaToile.Models.Match;
import DuelSurLaToile.Models.NavPaneWithSub;
import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.Handlers.ViewProfileActionHandler;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.util.List;
import java.util.Objects;

public class HistoryPane extends NavPaneWithSub {
    /**
     * Main
     */
    private final Main m;
    /**
     * List of user match history
     */
    private List<Match> matches;
    /**
     * The current user and the enemy user
     */
    private User curentUser, enemyUser;
    /**
     * Final view
     */
    private final VBox view;

    /**
     * HistoryPane constructor
     * @param m Main
     * @param u Curent user
     */
    public HistoryPane(Main m, User u) {
        super(m);
        this.m = m;
        this.curentUser = u;
        this.view = new VBox();
        this.prefWidthProperty().bind(this.m.getBodyPane().widthProperty().subtract(20));
    }

    /**
     * Setting a user for history pane
     * @param u Current user
     */
    public void setUser(User u) {
        this.curentUser = u;
    }

    /**
     * Refresh HistoryPane content
     */
    public void refresh() {
        this.view.getChildren().clear();
        this.getChildren().clear();
        if (Objects.nonNull(this.curentUser))
            this.matches = DatabaseMatch.getMatchByUser(this.curentUser);
        this.getChildren().add(createHistoryView());
        this.getStyleClass().add("history");
        System.out.println("refreshed history");
    }

    /**
     * @return View of all games
     */
    public VBox createHistoryView() {
        if (Objects.nonNull(this.matches))
            for (Match m : this.matches)
                view.getChildren().add(createGameHistoryView(m));
        VBox.setVgrow(this.view, Priority.ALWAYS);
        return this.view;
    }

    /**
     * @param m Main
     * @return View of a game
     */
    public HBox createGameHistoryView(Match m) {
        HBox game = new HBox();
        if (m.getUser1().getPseudo().equals(this.curentUser.getPseudo())) {
            this.curentUser = m.getUser1();
            this.enemyUser = m.getUser2();
        } else {
            this.curentUser = m.getUser2();
            this.enemyUser = m.getUser1();
        }
        if (Objects.isNull(this.curentUser) || Objects.isNull(this.enemyUser))      // if one of these is null, we can't create the history
            return new HBox();
        if (m.getScoreByUser(this.curentUser) == 1)
            game.getStyleClass().add("historyVictory");
        else
            game.getStyleClass().add("historyDefeat");
        Label versus = new Label("Versus");
        versus.getStyleClass().add("versusLabel");
        ToggleButton b = new ToggleButton("View Profile");
        b.setOnAction(new ViewProfileActionHandler(this.m, this.enemyUser));
        b.getStyleClass().add("button");
        game.getChildren().addAll(createGameInfo(m), versus, this.enemyUser.getImg(), createEnnemyInfo(m), new Label(m.getNumRound() + " round(s)"), b);
        return game;
    }

    /**
     * @param m Main
     * @return View of current user information
     */
    public VBox createGameInfo(Match m) {
        VBox gameInfo = new VBox();
        Label victory = new Label();
        if (m.getScoreByUser(this.curentUser) == 1) {
            victory.setText("Victory");
            victory.getStyleClass().add("historyVictoryLabel");
        } else {
            victory.setText("Defeat");
            victory.getStyleClass().add("historyDefeatLabel");
        }
        gameInfo.getChildren().addAll(victory, new Label("Quick Game"), new Label(m.getDateStart().toString()));
        return gameInfo;
    }

    /**
     * @param m Main
     * @return View of enemy user information
     */
    public VBox createEnnemyInfo(Match m) {
        VBox ennemyInfo = new VBox();
        Label enemyPseudo = new Label(this.enemyUser.getPseudo());
        enemyPseudo.getStyleClass().add("versusLabel");
        ennemyInfo.getChildren().addAll(new Label(this.enemyUser.getPseudo()), new Label(this.enemyUser.getWinCount() + " win(s)"), new Label(this.enemyUser.getLoseCount() + " loose(s)"));
        return ennemyInfo;
    }


}