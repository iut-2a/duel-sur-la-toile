package DuelSurLaToile.View;

import DuelSurLaToile.Database.DatabaseUser;
import DuelSurLaToile.Main;
import DuelSurLaToile.Models.Message;
import DuelSurLaToile.Models.User;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;


// todo

/**
 * It's the view of a Message
 */
public class MessagePane extends HBox {
    private final Label lbTxt;
    private final Label lbCurrentUser, lbOtherUser;
    private final User currentUser, otherUser;


    public MessagePane(Message msg, Main m) {
        this(msg.getContenumsg(), m.getCurrentUser(),DatabaseUser.getUserById(msg.getIdut_exp()),
                m.getCurrentUser().getPseudo().equals(DatabaseUser.getUserById(msg.getIdut_exp()).getPseudo()));    // todo ugly
    }


    public MessagePane(String text, User currentUser,User otherUser, boolean isCurrentUser) {
        super();
        this.getStyleClass().add("MessagePane");
        this.currentUser = currentUser;
        this.otherUser = otherUser;
        this.lbTxt = new Label(text);
        lbTxt.setWrapText(true);
        this.lbCurrentUser = new Label(currentUser.getPseudo() + " :");
        this.lbCurrentUser.getStyleClass().add("labelUser");
        this.lbOtherUser = new Label(": "+ otherUser.getPseudo());
        this.lbOtherUser.getStyleClass().add("labelUser");
//        this.getStyleClass().add("message");                    // todo
//        this.getStyleClass().add("message-" + (isCurrentUser ? "user" : "other"));
        this.setBackground(new Background(new BackgroundFill((isCurrentUser ? Color.LIGHTCORAL : Color.PALEVIOLETRED), CornerRadii.EMPTY, Insets.EMPTY)));  //todo in css

        // if the user is the current one, we make another disposition
        if (isCurrentUser)
            this.createCurrent();
        else
            this.createOther();

    }

    /**
     * add everything used to display the message
     * used when the message source is not the current user
     */
    private void createOther() {
        // can't do with UserPane // todo

        this.getChildren().addAll(lbTxt, lbOtherUser, this.otherUser.getImg());
        this.setAlignment(Pos.CENTER_RIGHT);
    }

    /**
     * add everything used to display the message
     * used when the message source is the current user
     */
    private void createCurrent() {
        this.getChildren().addAll(this.currentUser.getImg(), lbCurrentUser, lbTxt);
        this.setAlignment(Pos.CENTER_LEFT);

    }
}
