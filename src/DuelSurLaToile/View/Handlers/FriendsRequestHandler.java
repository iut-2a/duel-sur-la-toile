package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.User;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.sql.SQLException;

/**
 * Handler when the user want to add a new friend
 */
public class FriendsRequestHandler implements EventHandler<MouseEvent> {
    Main m;
    /**
     * the user who own the request
     */
    User user;
    /**
     * ?
     */
    boolean accept;

    public FriendsRequestHandler(Main m, User user, boolean accept) {
        super();
        this.m = m;
        this.user = user;
        this.accept = accept;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        if (this.m.isConnected()) {
            System.out.println(m.getCurrentUser());
            System.out.println(user);
            if (this.accept) {
                try {
                    User.acceptFriendRequest(user.getId(),m.getCurrentUser().getId());
                    User.addFriend(m.getCurrentUser(),user);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }


                System.out.println(user.getPseudo()+"a bien ete ajoute a votre liste d'amis");
            }
            else {
                try {
                    User.refuseFriendRequest(user.getId(),m.getCurrentUser().getId());
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
            }

            System.out.println(user.getPseudo()+"a bien ete suprime de votre liste de demande d'amis");
        }
    }

