package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.NavPaneWithSub;
import DuelSurLaToile.View.Util.BoardContent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;

/**
 * Event used when the user wants to start a new game of the connectFour
 */
public class PlayActionHandler implements EventHandler<ActionEvent> {
    private final Main m;

    public PlayActionHandler(Main main) {
        this.m = main;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        ToggleButton bt = (ToggleButton) actionEvent.getSource();
        Pane toSet = NavPaneWithSub.getViewToSet(this.m, BoardContent.valueOf(bt.getText()));
        this.m.getPlayPane().setView(toSet);
    }
}
