package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Database.DatabaseUser;
import DuelSurLaToile.Main;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.Objects;

public class ChangeImageHandler implements EventHandler<MouseEvent> {
    private final int userId;
    private final Main m;

    public ChangeImageHandler(Main m, int currentUserId) {
        super();
        this.m = m;
        this.userId = currentUserId;
    }


    @Override
    public void handle(MouseEvent mouseEvent) {
        FileChooser fileChooser = new FileChooser();
        // only jpg and png
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
        fileChooser.getExtensionFilters().add(extensionFilter);
        fileChooser.setTitle("Select your new Picture");
        File file = fileChooser.showOpenDialog(m.getStage());
        if (Objects.isNull(file))
            return;
        else
            DatabaseUser.setImage(userId, file);


    }
}
