package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Main;
import DuelSurLaToile.View.Util.ProfileContent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * Used when an user wants to see the history of someone else
 */
public class GoToHistoryHandler implements EventHandler<MouseEvent> {
    Main m;

    public GoToHistoryHandler(Main m) {
        super();
        this.m = m;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        if (this.m.isConnected()) {
            m.getBodyPane().setView(ProfileContent.History);
            m.refreshHeader();
            System.out.println("non");
//            m.refreshMatchHistory();
        }

    }
}
