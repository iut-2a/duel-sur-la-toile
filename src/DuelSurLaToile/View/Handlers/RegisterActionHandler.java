package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Database.DatabaseUser;
import DuelSurLaToile.Models.LoginModel;
import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.RegisterPane;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;

/**
 * Handler when the user wants to register
 */
public class RegisterActionHandler implements EventHandler<MouseEvent> {
    private final LoginModel loginModel;
    private final RegisterPane registerPane;
    private static final String stringRegex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*])(?=\\S+$).{8,15}$";

    public RegisterActionHandler(RegisterPane registerPane, LoginModel loginModel) {
        this.loginModel = loginModel;
        this.registerPane = registerPane;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        // Button bt = (Button) mouseEvent.getSource();
        this.handleRegister();
    }

    /**
     * try to register the user, if it's successful, switch to the homePane
     */
    private void handleRegister() {
        // make the registerPane the current pane
        System.out.println("Registering user");
        if (!this.registerPane.getPassword().matches(RegisterActionHandler.stringRegex)) {
            Alert alertePswd = new Alert(Alert.AlertType.ERROR);
            alertePswd.setTitle("Password Error");
            alertePswd.setContentText("Password is not correct\nPassword needs at least one capital letter, one number, 8 characters\n and a special character ");
            alertePswd.showAndWait();
        } else if (this.registerPane.getUsername().isEmpty() || this.registerPane.getEmail().isEmpty() || this.registerPane.getPassword().isEmpty()) {
            Alert alerte = new Alert(Alert.AlertType.ERROR);
            alerte.setTitle("Empty Fields error");
            alerte.setContentText("One ou severals Fields are empty");
            alerte.showAndWait();
        }
        else if (this.loginModel.attemptRegistration(this.registerPane.getUsername(), this.registerPane.getEmail(), this.registerPane.getPassword())) {
            System.out.println("Successfully created new user");
            User newUser = DatabaseUser.getUserByUserName(this.registerPane.getUsername());

        } else
            System.out.println("Failed to register new user");
    }

}