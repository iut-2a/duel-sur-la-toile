package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Models.Match;
import DuelSurLaToile.View.GamePane;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

/**
 * handle the time to wait for the user during the game
 */
public class WaitForGameHandler implements EventHandler<ActionEvent> {
    private final GamePane gamePane;
    private final boolean isUser1;
    private Match match;

    public WaitForGameHandler(GamePane gamePane, boolean isUser1) {
        super();
        this.gamePane = gamePane;
        this.isUser1 = isUser1;
    }

    @Override
    public void handle(ActionEvent actionEvent) {

        if (!this.gamePane.isActive())
            this.handleWaitForPlayer();
        else
            this.handleWaitForAction();
    }


    private void handleWaitForAction() {
        this.match = this.gamePane.getMatch();
        String lastState = match.getStatePa();
        this.match.update();
        String newState = match.getStatePa();
        if (newState.equals(lastState))
            return;
        else
            this.handlerPlayerAction(newState);
    }

    /**
     * Simply insert a pawn where the opponent did
     *
     * @param newState the newState
     */
    private void handlerPlayerAction(String newState) {
        String[] splittedState = newState.split(" ");
        if (splittedState.length < 2)    // no moves done
            return;
        int col = Integer.parseInt(splittedState[1]);
        this.gamePane.getConnectFourModel().insertPawn(col);
        if (splittedState[0].equals("Finished")) {
            this.gamePane.getConnectFourModel().win();
            this.gamePane.getConnectFourModel().setRunning(false);
        }


        this.match.update();

    }

    private void handleWaitForPlayer() {
        if (this.gamePane.askUpdatePlayer()) {
            this.gamePane.stopWaiting();
            this.gamePane.createGame();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText("An opponent has been found");
            alert.setContentText("Your match number " + this.gamePane.getGameNumber() + " is now ready!");

            Platform.runLater(alert::showAndWait);  // because can't do this during an animation

        } else
            System.out.println("Still waiting for a new player");
    }
}
