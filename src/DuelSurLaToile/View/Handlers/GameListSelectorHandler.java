package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.View.PlayPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;

public class GameListSelectorHandler implements EventHandler<ActionEvent> {
    private final PlayPane playPane;

    public GameListSelectorHandler(PlayPane playPane) {
        super();
        this.playPane = playPane;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        ComboBox source = (ComboBox) actionEvent.getSource();
        String currentGameString = (String) source.getSelectionModel().getSelectedItem();
        String[] splittedGameString = currentGameString.split(" ");

        // -1 at the end because list indexes starts at 0
        int currentGame = Integer.parseInt(splittedGameString[splittedGameString.length - 1]) - 1;

        this.playPane.setGame(currentGame);
        System.out.println("Game " + currentGameString + " selected");

    }
}
