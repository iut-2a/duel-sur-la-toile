package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Database.DatabaseUser;
import DuelSurLaToile.Main;
import DuelSurLaToile.View.AdminPanel;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;

/**
 * Used when the user click on his avatar or the main title
 * Redirect the user to the homePage
 */
public class AdminHandler implements EventHandler<MouseEvent> {
    Main m;
    Boolean bool;
    AdminPanel adminPanel;

    public AdminHandler(Main m, Boolean bool, AdminPanel adminPanel) {
        super();
        this.m = m;
        this.bool = bool;
        this.adminPanel = adminPanel;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        // have to be connected or do nothing
        String user = this.adminPanel.getUsername();
        if (this.m.isConnected() && this.m.getCurrentUser().isAdmin()) {
            if (bool) {
                DatabaseUser.activateUser(user);
                alertact();
            } else {
                DatabaseUser.desactivateUser(user);
                alertdes();
            }
        }
    }

    public void alertact() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Admin panel");
        alert.setContentText(this.adminPanel.getUsername()+" has been enabled");
        Platform.runLater(alert::showAndWait);  // because can't do this during an animation
    }

    public void alertdes() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Admin panel");
        alert.setContentText(this.adminPanel.getUsername()+" has been disabled");
        Platform.runLater(alert::showAndWait);  // because can't do this during an animation
    }
}