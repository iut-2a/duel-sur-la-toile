package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.NavPaneWithSub;
import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.ProfilePane;
import DuelSurLaToile.View.Util.OwnProfileContent;
import DuelSurLaToile.View.Util.ProfileContent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;

/**
 * Used for subNavBars in the profiles, when we click on one
 */
public class ViewProfileActionHandler implements EventHandler<ActionEvent> {
    private final Main m;
    private User u;

    public ViewProfileActionHandler(Main main, User user) {
        this.m = main;
        this.u = user;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        ToggleButton bt = (ToggleButton) actionEvent.getSource();
        Pane toSet = new Pane();
        try {
            toSet = NavPaneWithSub.getViewToSet(this.m, ProfileContent.valueOf(bt.getText()));
        } catch (IllegalArgumentException e) {  // value not in profileContent so it's in OwnProfileContent
            if (!bt.getText().equals("View Profile"))
                toSet = NavPaneWithSub.getViewToSet(this.m, OwnProfileContent.valueOf(bt.getText()));
        }
        if (bt.getText().equals("View Profile")){
//            this.m.getProfilePane().refresh();
            this.m.setOtherUserProfile(new ProfilePane(this.m, this.u));
            this.m.getBodyPane().setView(this.m.getOtherUserProfile());
            this.m.getOtherUserProfile().setView(toSet);
        }

        if (bt.getText().equals("History")){
            this.m.refreshOtherUserMatchHistory();
            this.m.getOtherUserProfile().setView(this.m.getOtherUserProfile().getHistoryPane());
        }
        this.m.refreshSubHeader();

    }
}
