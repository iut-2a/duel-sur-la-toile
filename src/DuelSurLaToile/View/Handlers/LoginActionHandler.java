package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Database.DatabaseUser;
import DuelSurLaToile.Models.LoginModel;
import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.LoginPane;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;

import static DuelSurLaToile.View.Util.VisitorHeaderContent.Login;
import static DuelSurLaToile.View.Util.VisitorHeaderContent.Register;

/**
 * Used for any action that the user tries to do in the login screen
 */
public class LoginActionHandler implements EventHandler<MouseEvent> {
    private final LoginModel loginModel;
    private final LoginPane loginPane;

    public LoginActionHandler(LoginPane loginPane, LoginModel loginModel) {
        super();
        this.loginModel = loginModel;
        this.loginPane = loginPane;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        Button bt = (Button) mouseEvent.getSource();
        String txt = bt.getText();
        if (txt.equals(Login.getValue()))
            this.handleLogin();
        else if (txt.equals(Register.getValue()))
            this.handleRegister();
        else
            System.out.println("unknown button: " + txt);
    }

    /**
     * When the user tries to log in with his credential
     * try to connect the user
     */
    private void handleLogin() {
        System.out.println("Login handling");
        if (this.loginModel.attemptConnection(this.loginPane.getUsername(), this.loginPane.getPassword())) {
            User newUser = DatabaseUser.getUserByUserName(this.loginPane.getUsername());
            if (newUser.isActive())
                this.loginModel.setUser(newUser);
            else
                System.out.println("User disabled");
        } else {
            // todo inform
            Alert alertelogin = new Alert(Alert.AlertType.ERROR);
            alertelogin.setTitle("Failed to login");
            alertelogin.setContentText("Wrong password or username");
            alertelogin.showAndWait();
            System.out.println("Wrong password or username");
        }
    }

    /**
     * when the user wants to register, switch to the register pane
     */
    private void handleRegister() {
        // make the registerPane the current pane
        System.out.println("Switching to register");
        this.loginPane.setRegisterPane();
    }

}