package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.View.PlayPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * handle the game creation
 */
public class GameStartHandler implements EventHandler<ActionEvent> {
    private final PlayPane playPane;

    public GameStartHandler(PlayPane playPane) {
        super();
        this.playPane = playPane;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        this.playPane.startNewGame();

    }
}
