package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.NavPaneWithSub;
import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.ProfilePane;
import DuelSurLaToile.View.Util.OwnProfileContent;
import DuelSurLaToile.View.Util.ProfileContent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;

import java.awt.*;

/**
 * Used for subNavBars in the profiles, when we click on one
 */
public class ProfileActionHandler implements EventHandler<ActionEvent> {
    private final Main m;
    private User u;

    public ProfileActionHandler(Main main, User u) {
        this.m = main;
        this.u = u;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        ToggleButton bt = (ToggleButton) actionEvent.getSource();
        Pane toSet;
        try {
            toSet = NavPaneWithSub.getViewToSet(this.m, ProfileContent.valueOf(bt.getText()));
        } catch (IllegalArgumentException e) {  // value not in profileContent so it's in OwnProfileContent
//            if (!bt.getText().equals("View Profile"))
            toSet = NavPaneWithSub.getViewToSet(this.m, OwnProfileContent.valueOf(bt.getText()));
        }
//        if (bt.getText().equals("View Profile")) {
//            this.m.setOtherUserProfile(new ProfilePane(this.m, this.u));
//            this.m.getBodyPane().setView(this.m.getOtherUserProfile());
//            this.m.getOtherUserProfile().setView(toSet);
//        } else
        this.m.getProfilePane().setView(toSet);
        if (bt.getText().equals("History")) {
            if (this.m.getBodyPane().getView().equals(this.m.getOtherUserProfile())) {
                this.m.refreshOtherUserMatchHistory();
                this.m.getOtherUserProfile().setView(this.m.getOtherUserProfile().getHistoryPane());
            } else this.m.refreshMatchHistory();
        }
        if (bt.getText().equals("Overview")) {
            this.m.getProfilePane().setView(this.m.getOverviewPane());
            this.m.refreshOverview();
//            if (this.m.getBodyPane().getView().equals(this.m.getOtherUserProfile())) {
//                this.m.refreshOtherUserOverview();
//                this.m.getOtherUserProfile().setView(this.m.getOtherUserProfile().getOverviewPane());
//            } else this.m.refreshOverview();
        }
        this.m.refreshSubHeader();

    }
}
