package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Main;
import DuelSurLaToile.View.Util.AdminHeaderContent;
import DuelSurLaToile.View.Util.UserHeaderContent;
import DuelSurLaToile.View.Util.VisitorHeaderContent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ToggleButton;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * Handler of the main navbar
 * Used to switch between every panes
 */
public class NavBarHandlerAction implements EventHandler<ActionEvent> {
    private final Main m;

    public NavBarHandlerAction(Main m) {
        super();
        this.m = m;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        ToggleButton bt = (ToggleButton) actionEvent.getSource();
        Class<? extends Enum> enu;
        if (Objects.isNull(this.m.getCurrentUser()))
            enu = VisitorHeaderContent.class;
        else if (bt.getText().startsWith("Admin"))       // because admins have the userHeaderContents value and the Admin ones
            enu = AdminHeaderContent.class;
        else
            enu = UserHeaderContent.class;
        Method valueOf;
        // we retrieve the valueOf method  of enu
        try {
            valueOf = enu.getMethod("valueOf", Class.class, String.class);
        } catch (NoSuchMethodException e) {
            // shouldn't have this with modern enums
            System.out.println("bad Interface chosen in NavBarHandlerAction");
            e.printStackTrace();
            return;
        }
        try {
            this.m.getBodyPane().setView(valueOf.invoke(enu, enu, bt.getText()));
        } catch (IllegalAccessException | InvocationTargetException e) {
            System.out.println("error in NavBarHandlerAction with button: " + bt.getText());
            System.out.println("the chosen interface may not have this constant");
            e.printStackTrace();
        }
        this.m.refreshHeader();
        if(bt.getText().equals("Home"))
            this.m.getHomePane().refresh();
    }
}
