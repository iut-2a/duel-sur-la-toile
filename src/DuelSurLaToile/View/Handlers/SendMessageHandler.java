package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Database.DatabaseMessage;
import DuelSurLaToile.View.MessageUserPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Represents the event when the user want to send a new message from the socialPane
 */
public class SendMessageHandler implements EventHandler<ActionEvent> {
    private final MessageUserPane messageUserPane;

    public SendMessageHandler(MessageUserPane messageUserPane) {
        this.messageUserPane = messageUserPane;
    }


    @Override
    public void handle(ActionEvent actionEvent) {
        this.messageUserPane.getMessage();
        DatabaseMessage.addMessage(this.messageUserPane.getSourceUser().getId(), this.messageUserPane.getDestUser().getId(), this.messageUserPane.getMessage());
        this.messageUserPane.clearMessage();
        // we refresh the msg list
        this.messageUserPane.setUser(this.messageUserPane.getDestUser());
    }

}
