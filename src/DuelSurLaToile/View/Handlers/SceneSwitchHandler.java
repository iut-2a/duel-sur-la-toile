package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.ContentInterface;
import DuelSurLaToile.View.Util.HeaderContent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * Used when the user click on his avatar or the main title
 * Redirect the user to the homePage
 */
public class SceneSwitchHandler implements EventHandler<MouseEvent> {
    private final Main m;
    private final ContentInterface where;

    public SceneSwitchHandler(Main m, HeaderContent where) {
        super();
        this.m = m;
        this.where = where;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        // have to be connected or do nothing
        if (this.m.isConnected()) {
            m.getBodyPane().setView(where);
            m.refreshHeader();
        }

    }
}