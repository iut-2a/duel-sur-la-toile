package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Main;
import DuelSurLaToile.View.Util.UserHeaderContent;
import DuelSurLaToile.View.Util.VisitorHeaderContent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * Used when the user click on his avatar or the main title
 * Redirect the user to the homePage
 */
public class LogoutHandler implements EventHandler<MouseEvent> {
    Main m;

    public LogoutHandler(Main m) {
        super();
        this.m = m;
    }

    @Override
    public void handle(MouseEvent mouseEvent) {
        this.m.logout();
        m.getBodyPane().setView(VisitorHeaderContent.Login);
        m.refreshHeader();
    }
}