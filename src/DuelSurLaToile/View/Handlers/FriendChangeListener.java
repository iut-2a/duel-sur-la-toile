package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.SocialPane;
import DuelSurLaToile.View.UserPane;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 * Used when the user select a friend in SocialPane
 */
public class FriendChangeListener implements ChangeListener<UserPane> {
    private final SocialPane socialPane;

    public FriendChangeListener(SocialPane socialPane) {
        super();
        this.socialPane = socialPane;
    }

    @Override
    public void changed(ObservableValue<? extends UserPane> observable, UserPane oldUserPane, UserPane newUserPane) {
        User currentUser = newUserPane.getUser();
        this.socialPane.setSelected(currentUser);

    }
}
