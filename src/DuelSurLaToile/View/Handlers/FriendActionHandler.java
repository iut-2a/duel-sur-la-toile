package DuelSurLaToile.View.Handlers;

import DuelSurLaToile.Database.DatabaseFriend;
import DuelSurLaToile.Database.DatabaseUser;
import DuelSurLaToile.Main;
import DuelSurLaToile.Models.FriendRequest;
import DuelSurLaToile.Models.NavPaneWithSub;
import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.Util.BoardContent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;

import java.sql.SQLException;
import java.util.Objects;

/**
 * Event used when the user wants to start a new game of the connectFour
 */
public class FriendActionHandler implements EventHandler<ActionEvent> {
    private final Main m;
    private TextField tf;

    public FriendActionHandler(Main main, TextField tf) {
        this.m = main;
        this.tf = tf;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        ToggleButton bt = (ToggleButton) actionEvent.getSource();
        if (bt.getText().equals("Add friend")){
            try {
                DatabaseFriend.addFriendInvitation(new FriendRequest(this.m.getCurrentUser(),DatabaseUser.getUserByUserName(tf.getText())));
                System.out.println("You sent a friendship request to " + tf.getText());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        else if(bt.getText().equals("Remove friend")){
            try {
                DatabaseFriend.removeFriend(this.m.getCurrentUser(), Objects.requireNonNull(DatabaseUser.getUserByUserName(tf.getText())));
                System.out.println("You removed " + tf.getText() + "from your friend list");
                this.m.getSocialPane().refreshFriendList();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
