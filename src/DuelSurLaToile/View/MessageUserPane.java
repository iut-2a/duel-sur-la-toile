package DuelSurLaToile.View;

import DuelSurLaToile.Database.DatabaseMessage;
import DuelSurLaToile.Main;
import DuelSurLaToile.Models.Message;
import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.Handlers.SendMessageHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MessageUserPane extends VBox {
    private final SocialPane socialPane;
    private final VBox chat;
    private final HBox messageBox;
    private final TextArea msgTextBox;
    private final Button sendButton;
    /**
     * current Destination user
     */
    private User userDest;

    public MessageUserPane(Main m, SocialPane socialPane) {
        super();
        Label title = new Label("Chat: ");
        title.getStyleClass().add("socialLabel");
        this.chat = new VBox();
        this.messageBox = new HBox();
        this.getChildren().addAll(title, this.chat, this.messageBox);

//         this.setFitToWidth(true);
        this.socialPane = socialPane;
        this.msgTextBox = new TextArea();
        this.sendButton = new Button("Send");

        this.sendButton.setOnAction(new SendMessageHandler(this));
        this.messageBox.setStyle("-fx-alignment: center");
        this.messageBox.setBackground(new Background(new BackgroundFill(Color.GRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        this.messageBox.getChildren().addAll(this.msgTextBox, this.sendButton);

        this.prefWidthProperty().bind(m.getP().widthProperty().multiply(0.75).subtract(20));
        this.prefHeightProperty().bind(m.getP().heightProperty().divide(2));


    }


    public void setUser(User userToSet) {
        this.chat.getChildren().clear();
        this.userDest = userToSet;
        List<Message> msgtoShow = DatabaseMessage.getAllMessage(this.socialPane.getCurrentUser(), userToSet);
        Collections.reverse(msgtoShow);
        for (Message msg : msgtoShow) {
            MessagePane toAdd = new MessagePane(msg, this.socialPane.getMain());
            toAdd.prefHeightProperty().bind(this.chat.heightProperty().multiply(0.2));
            toAdd.prefWidthProperty().bind(this.socialPane.getMain().getScene().widthProperty());
            this.chat.getChildren().add(toAdd);
        }
    }

    public String getMessage() {
        return this.msgTextBox.getText();
    }

    public void clearMessage() {
        this.msgTextBox.setText("");
    }

    public User getDestUser() {
        return this.userDest;
    }

    public User getSourceUser() {
        return this.socialPane.getCurrentUser();
    }
}
