package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.Handlers.FriendsRequestHandler;
import DuelSurLaToile.View.Util.UserAvatar;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

import javax.swing.*;

public class InvitationPane extends HBox {
    private final User user;
    private Main m;

    public InvitationPane(User user,Main m) {
        super();
        this.user = user;
        this.m = m;
        this.getStyleClass().add("invite");
        this.setAlignment(Pos.CENTER);
        UserAvatar img = user.getImg();
        img.setRadius(38);
        img.getStyleClass().add("HeaderAvatar");
        Label pseudo = new Label(user.getPseudo() + " sent you an invitation");
        Button Accept = new Button("Accept");
        Accept.setOnMouseClicked(new FriendsRequestHandler(this.m,user,true));
        Accept.getStyleClass().add("accept");
        Button Decline = new Button("Decline");
        Decline.setOnMouseClicked(new FriendsRequestHandler(this.m,user,false));
        Decline.getStyleClass().add("decline");
        this.getChildren().addAll(img, pseudo, Accept, Decline);
    }
}
