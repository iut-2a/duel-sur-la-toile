package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.NavPaneWithSub;
import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.Handlers.ProfileActionHandler;
import DuelSurLaToile.View.Handlers.ViewProfileActionHandler;
import DuelSurLaToile.View.Util.OwnProfileContent;
import DuelSurLaToile.View.Util.ProfileContent;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

/**
 * view for the Profile
 * Should have a getMethod for every value in the ProfileContent enum
 */
public class ProfilePane extends NavPaneWithSub {
    // todo   should extends a Pane but probably not this one
    /**
     * A profile always belong to someone
     */
    private final VBox navBox;
    /**
     * Main
     */
    private final Main m;
    /**
     * An user
     */
    private User u;
    /**
     * Social view
     */
    private final SocialPane social;
    /**
     * History view
     */
    private final HistoryPane historyPane;
    /**
     * Overview view
     */
    private final OverviewPane overviewPane;

    /**
     * ProfilePane constructor when user is not the current user and when it is
     * @param m Main
     * @param u User
     */
    public ProfilePane(Main m, User u) {
        super(m);
        this.m = m;
        this.u = u;
        super.createButtons(ProfileContent.values(), new ProfileActionHandler(this.m, u));
        this.navBox = super.getNavBox();
        this.social = new SocialPane(this.m);
        this.historyPane = new HistoryPane(this.m, u);
        this.overviewPane = new OverviewPane(this.m);
        this.getStyleClass().add("profile");
        this.getChildren().add(this.navBox);

        // default selection is the Overview
        super.setSelected(super.getButtonByName(ProfileContent.Overview.getValue()));

    }

    /**
     * Refresh overview view
     */
    public void refresh() {
        if(this.m.isConnected()){
            this.overviewPane.refresh();
        }
    }

    /**
     * Change the ProfilePane user
     * @param u User
     */
    public void setUser(User u) {
        if (this.m.getCurrentUser().equals(u))
            super.createButtons(ProfileContent.values(), new ProfileActionHandler(this.m, this.u));
        else
            super.createButtons(ProfileContent.values(), new ViewProfileActionHandler(this.m, this.u));
    }
    public User getUser(){
        return this.u;
    }

    /**
     * When the Profile is of the current user
     *
     * @param m
     */
    public ProfilePane(Main m) {
        this(m, m.getCurrentUser());
        super.createButtons(OwnProfileContent.values(), new ProfileActionHandler(this.m, this.m.getCurrentUser()));
        // keep the default selection for the overview
        super.setSelected(super.getButtonByName(ProfileContent.Overview.getValue()));

    }

    /**
     * @return return SocialPane
     */
    public SocialPane getSocialPane() {
        return this.social;
    }
    /**
     * @return return HistoryPane
     */
    public HistoryPane getHistoryPane() {
        return this.historyPane;
    }
    /**
     * @return return OverviewPane
     */
    public OverviewPane getOverviewPane() {
        return this.overviewPane;
    }

}