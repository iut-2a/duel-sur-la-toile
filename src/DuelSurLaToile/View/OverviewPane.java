package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.View.Handlers.ChangeImageHandler;
import DuelSurLaToile.View.Util.UserAvatar;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.text.DecimalFormat;

public class OverviewPane extends BorderPane {
    /**
     * Main
     */
    private final Main m;
    /**
     * A VBox that contain the left part of the vue
     */
    private final VBox left;
    /**
     * A VBox that contain the right part of the vue
     */
    private final VBox right;

    /**
     * OverviewPane constructor
     *
     * @param m Main
     */
    public OverviewPane(Main m) {
        super();
        this.m = m;
        this.left = new VBox();
        this.right = new VBox();
        this.setLeft(left);
        this.setRight(right);
        this.prefWidthProperty().bind(this.m.getP().widthProperty().subtract(20));
        this.getStyleClass().add("overview");
    }

    /**
     * Refresh OverviewPane content
     */
    public void refresh() {
        if (this.m.isConnected()) {
            refreshRight();
            refreshLeft();
        }
    }

    /**
     * Refresh the left part of the Overview
     */
    private void refreshLeft() {
        this.left.getChildren().clear();
        Label name = new Label(this.m.getCurrentUser().getPseudo());
        name.getStyleClass().add("name");
        Label email = new Label(this.m.getCurrentUser().getEmail());
        UserAvatar img = this.m.getCurrentUser().getImg();
        img.getStyleClass().add("HeaderAvatar");
        img.setOnMouseClicked(new ChangeImageHandler(this.m, this.m.getCurrentUser().getId()));

        HBox user = new HBox();
        user.getChildren().addAll(img, name);
        this.left.getChildren().addAll(user, email);
        left.getStyleClass().add("left");
        ReadOnlyDoubleProperty width = this.m.getScene().widthProperty();
        ReadOnlyDoubleProperty height = this.m.getScene().heightProperty();
        this.left.prefWidthProperty().bind(width.multiply(0.46));
        this.left.prefHeightProperty().bind(height.divide(10));
    }

    /**
     * Refresh the right part of the Overview
     */
    private void refreshRight() {
        this.right.getChildren().clear();
        DecimalFormat df2 = new DecimalFormat("#.##");
        Label winrate = new Label("Win rate");
        Label wr = new Label(df2.format(this.m.getCurrentUser().getWinRate()) + " %");
        wr.getStyleClass().add("wr");
        Label gamesplayed = new Label("Games played");
        Label numbergames = new Label(df2.format(this.m.getCurrentUser().getNbGame()));
        numbergames.getStyleClass().add("wr");
        right.getStyleClass().add("left");
        HBox games = new HBox();
        games.getChildren().addAll(winrate, wr);
        HBox gms = new HBox();
        gms.getChildren().addAll(gamesplayed);
        HBox win = new HBox();
        win.getChildren().add(numbergames);
        HBox nb = new HBox();
        nb.getChildren().add(wr);
        nb.getStyleClass().add("win");
        this.right.getChildren().addAll(games, nb, gms, win);
        ReadOnlyDoubleProperty width = this.m.getScene().widthProperty();
        ReadOnlyDoubleProperty height = this.m.getScene().heightProperty();
        this.right.prefWidthProperty().bind(width.multiply(0.46));
        this.right.prefHeightProperty().bind(height.divide(10));
        win.getStyleClass().add("win");
    }
}


