package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.View.Handlers.AdminHandler;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;


public class AdminPanel extends VBox {
    /**
     * Main
     */
    private final Main m;
    /**
     * TextField for write the user name to Deactivate/Activate
     */
    private TextField texte;

    /**
     * Constructor of the admin panel
     *
     * @param m Main
     */
    public AdminPanel(Main m) {
        super();
        this.m = m;
    }

    /**
     * Refresh the admin panel
     */
    public void refresh() {
        if (this.m.isConnected() && this.m.getCurrentUser().isAdmin()) {
            this.getChildren().clear();
            HBox button = new HBox();
            Label admin = new Label("Welcome to the administration interface");
            admin.setFont(new Font("Arial", 24));
            this.texte = new TextField();
            texte.setPromptText("Type username here");
            Button desactiver = new Button("Deactivate a player");
            Label lmh = new Label(texte.getText());
            desactiver.setOnMouseClicked(new AdminHandler(this.m, false, this));
            Button activer = new Button("Activate a player");
            activer.setOnMouseClicked(new AdminHandler(this.m, true, this));
            button.getChildren().addAll(activer, desactiver);
            this.getChildren().addAll(admin, texte, lmh, button);
            this.getStyleClass().add("admincss");
            button.getStyleClass().add("boutton");
            ReadOnlyDoubleProperty width = this.m.getScene().widthProperty();
            ReadOnlyDoubleProperty height = this.m.getScene().heightProperty();
            this.prefWidthProperty().bind(width);
            this.prefHeightProperty().bind(height.divide(10));
        }
    }

    /**
     * @return Return the text that been write in the Textfield
     */
    public String getUsername() {
        return texte.getText();
    }
}
