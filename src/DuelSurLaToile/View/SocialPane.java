package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.User;
import javafx.scene.layout.HBox;

/**
 * Contains the view of the user's friendlist and the messages associated to the selected user ( when there is one)
 */
public class SocialPane extends HBox {
    private final Main m;
    /**
     * The message associated to the selected user
     */
    private final MessageUserPane msgBox;
    /**
     * the User who owns these messages
     */
    private User currentUser;
    /**
     * All friends of the current user ( actually it contains every users registered ) // todo
     */
    private FriendListPane friendL;


    public SocialPane(Main m) {
        super();
        this.m = m;
        this.getStyleClass().add("social");
        this.msgBox = new MessageUserPane(m,this);
    }

    /**
     * Initialize only when the user is logged in
     */
    public void initializeSocial() {
        this.currentUser = this.m.getCurrentUser();

        // todo
        this.friendL = new FriendListPane(this.m, this, this.currentUser.getFriendList());
        // this.friendL = new FriendListPane(this, this.currentUser.getFriendList());
        this.getChildren().clear();
        this.getChildren().addAll(friendL, msgBox);

    }

    public MessageUserPane getMessageUserPane() {
        return this.msgBox;
    }

    /**
     * @return the users who belons this socialPane
     */
    public User getCurrentUser() {
        return this.currentUser;
    }

    public FriendListPane getFriendL() {
        return friendL;
    }

    /**
     * make the msgBox showing the currentUser messages
     *
     * @param currentUser the friend who the currentUser selected
     */
    public void setSelected(User currentUser) {
        System.out.println(currentUser.getPseudo() + " is now selected");
        this.msgBox.setUser(currentUser);
        //friendL.prefHeightProperty().bind(this.msgBox.heightProperty());

    }
    public void refreshFriendList(){
        this.friendL.refreshFriendsList(this.currentUser.getFriendList());
    }

    public Main getMain() {
        return this.m;
    }

}