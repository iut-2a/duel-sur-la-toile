
package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.LoginModel;
import DuelSurLaToile.View.Handlers.RegisterActionHandler;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class RegisterPane extends VBox {
    private final Main m;
    private TextField username;
    private TextField email;
    private PasswordField password,confirmpassword;
    private Button validate;
    private CheckBox acceptpolicy;

    public RegisterPane(Main m) {
        super();
        this.m = m;
        this.getStyleClass().add("LoginPane");
        this.initializeForms();

        this.setMinSize(800, 600);
//        this.setPadding(new Insets(15, 12, 15, 12));
//        this.setSpacing(10);
    }

    private void initializeForms() {
        this.createTitle();
        this.createUserForm();
        this.createEmailForm();
        this.createPasswordForm();
        this.createPasswordConfirmForm();
        this.terms();
        this.createButtonsForm();

    }


    /**
     * creates and add forms
     */

    private void createTitle(){
        HBox titrebox = new HBox();
        Label titre = new Label("Sign Up ! \nIt's Quick and easy !");
        titre.setFont(new Font("Arial", 36));
        titrebox.getChildren().addAll(titre);
        titrebox.setAlignment(Pos.CENTER);
        this.getChildren().add(titrebox);

    }
    private void createUserForm() {
        HBox user = new HBox();
        this.username = new TextField();
        this.username.setPromptText("Username");
        this.username.setPrefSize(200,40);
        user.getChildren().addAll(this.username);
        user.setAlignment(Pos.CENTER);
        this.getChildren().add(user);
    }

    private void createEmailForm() {
        HBox email = new HBox();
        this.email = new TextField();
        this.email.setPromptText("Email");
        this.email.setPrefSize(200,40);
        email.getChildren().addAll(this.email);
        email.setAlignment(Pos.CENTER);
        this.getChildren().add(email);
    }

    private void createPasswordForm() {
        HBox passwd = new HBox();
        this.password = new PasswordField();
        this.password.setPromptText("Password");
        this.password.setPrefSize(200,40);
        passwd.getChildren().addAll( this.password);
        this.getChildren().add(passwd);
    }

    private void createPasswordConfirmForm() {
        HBox confpasswd = new HBox();
        this.confirmpassword = new PasswordField();
        this.confirmpassword.setPromptText("Confirm Password");
        this.confirmpassword.setPrefSize(200,40);
        confpasswd.getChildren().addAll(this.confirmpassword);
        this.getChildren().add(confpasswd);
    }

    private void terms(){
        HBox term = new HBox();
        Label confirmPolicy = new Label("Accept terms of services and security\nread the terms:...");
        this.acceptpolicy = new CheckBox();
        term.getChildren().addAll(this.acceptpolicy,confirmPolicy);
        this.getChildren().add(term);
    }

    /**
     * initialize the button form and add it to this
     */

    private void createButtonsForm() {
        HBox buttons = new HBox();
        LoginModel log = new LoginModel(this.m);
        this.validate = new Button("Validate");
        var registerAction = new RegisterActionHandler(this, log);
        this.validate.setOnMouseClicked(registerAction);
        buttons.getChildren().addAll(validate);
        this.getChildren().add(buttons);
    }

    public String getUsername() {
        return this.username.getText();
    }

    public String getPassword() {
        return this.password.getText();
    }

    public String getEmail() {
        return this.email.getText();
    }
}

