package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.FriendRequest;
import DuelSurLaToile.Models.User;
import javafx.scene.layout.GridPane;


import java.util.List;

import static DuelSurLaToile.Database.DatabaseFriend.getFriendRequestForUser;

public class InvitationListPane extends GridPane {
    private final Main m;
    private List<User> listuser;

    public InvitationListPane(Main m) {
        super();
        this.m = m;
        List<FriendRequest> userInvitations = getFriendRequestForUser(m.getCurrentUser());
        int x = 0;

        for (FriendRequest friendRequest : userInvitations) {
            this.add(new InvitationPane(friendRequest.getUserexp(),this.m), 0, x);
            ++x;
        }
    }

}
