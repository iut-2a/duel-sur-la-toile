package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.View.Handlers.FriendActionHandler;
import DuelSurLaToile.View.Handlers.LogoutHandler;
import DuelSurLaToile.View.Util.UserAvatar;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.text.DecimalFormat;

/**
 * view for the Home
 */

public class HomePane extends BorderPane {
    /**
     * Main
     */
    private final Main m;
    /**
     * It's a string that contains the name of the player who is connected
     */
    private final Label lbWelcolme;
    /**
     * a vbox that contains the left part of the view
     */
    private final VBox left;
    /**
     * a vbox that contains the right part of the view
     */
    private final VBox right;


    /**
     * HomePane constructor
     *
     * @param m Main
     */
    public HomePane(Main m) {
        super();
        this.m = m;
        this.lbWelcolme = new Label();
        this.lbWelcolme.getStyleClass().add("welcome");
        this.left = new VBox();
        this.right = new VBox();
        this.setLeft(left);
        this.setRight(right);
        this.prefWidthProperty().bind(this.m.getP().widthProperty());
        this.getStyleClass().add("home");
        this.left.getStyleClass().add("left");
        this.right.getStyleClass().add("right");
    }

    /**
     * Refresh HomePane content
     */
    public void refresh() {
        if (this.m.isConnected()) {

            // this.lbWelcolme.setText("Welcome here " + this.m.getCurrentUser().getPseudo() + " !");
            refreshRight();
            refreshLeft();
        }
    }

    /**
     * Refresh the left part of the homePane
     */
    private void refreshLeft() {
        this.left.getChildren().clear();


        HBox profileView = new HBox();
        UserAvatar imgUser = this.m.getCurrentUser().getImg();
        Label lbUser = new Label(this.m.getCurrentUser().getPseudo());
        lbUser.setFont(new Font("Arial", 24));
        profileView.getChildren().addAll(imgUser, lbUser);

        Label invite = new Label("Pending invitations");
        invite.getStyleClass().add("invitLab");
        InvitationListPane listinvit = new InvitationListPane(this.m);

        HBox addfriend = new HBox();
        TextField tfAddFriend = new TextField();
        tfAddFriend.setPromptText("Pseudo");
        ToggleButton btAddFriend = new ToggleButton("Add friend");
        btAddFriend.setOnAction(new FriendActionHandler(this.m, tfAddFriend));
        addfriend.getChildren().addAll(tfAddFriend,btAddFriend);

        this.left.getChildren().addAll(profileView, addfriend,invite ,listinvit);
        ReadOnlyDoubleProperty width = this.m.getScene().widthProperty();
        ReadOnlyDoubleProperty height = this.m.getScene().heightProperty();
        this.left.prefWidthProperty().bind(width.multiply(0.49));
        this.left.prefHeightProperty().bind(height.divide(10));
    }

    /**
     * Refresh the right part of the homePane
     */
    private void refreshRight() {
        this.right.getChildren().clear();
        DecimalFormat df2 = new DecimalFormat("#.##");
        Button logout = new Button("Logout");
        logout.getStyleClass().add("logout");
        logout.setOnMouseClicked(new LogoutHandler(this.m));
        Label wr = new Label(df2.format(this.m.getCurrentUser().getWinRate()) + " %");
        wr.getStyleClass().add("wr");
        VBox center = new VBox();
        center.getChildren().addAll(wr, logout);
        this.right.getChildren().addAll(new Label("Win rate"), center);
        center.getStyleClass().add("center");
        ReadOnlyDoubleProperty width = this.m.getScene().widthProperty();
        ReadOnlyDoubleProperty height = this.m.getScene().heightProperty();
        this.right.prefWidthProperty().bind(width.multiply(0.40));
        this.right.prefHeightProperty().bind(height.divide(10));
    }
}