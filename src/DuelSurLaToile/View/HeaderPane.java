package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.NavPane;
import DuelSurLaToile.View.Handlers.NavBarHandlerAction;
import DuelSurLaToile.View.Handlers.SceneSwitchHandler;
import DuelSurLaToile.View.Util.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.util.Objects;

/**
 * Contains the navbar(with the buttons)
 */
public class HeaderPane extends NavPane {
    private VBox titles;
    private final HBox navBox;
    private ToggleGroup gp;
    private final Main m;

    /**
     *
     */
    public HeaderPane(Main m) {
        super(m);
        this.m = m;
        this.navBox = super.getNavBox();
        //ReadOnlyDoubleProperty width = this.m.getScene().widthProperty();
        //ReadOnlyDoubleProperty height = this.m.getScene().heightProperty();

        this.setSpacing(10.);

        this.getStyleClass().add("header");
        this.getChildren().addAll(this.left(), this.right());


        //this.prefWidthProperty().bind(width);
        //this.prefHeightProperty().bind(height.divide(10));

        //this.fontSize = new SimpleDoubleProperty(10);
        //this.fontSize.bind(width.add(height).divide(75));
        //this.styleProperty().bind(Bindings.concat("-fx-font-size: ", fontSize.asString(), ";"));
    }

    /**
     * @return a pane that contains the left side of the header
     */
    private Pane left() {
        this.titles = new VBox();
        Label title = new Label("Duel sur la toile");
        Label subTitle = new Label("Connect Four");
        title.setTextFill(Color.WHITE);
        subTitle.setTextFill(Color.WHITE);
        titles.getChildren().addAll(title, subTitle);
        HBox.setHgrow(this.titles, Priority.ALWAYS);
        this.titles.setAlignment(Pos.CENTER_LEFT);
        this.titles.setOnMouseClicked(new SceneSwitchHandler(this.m, HeaderContent.Home));
        this.titles.getStyleClass().add("HeaderTitles");
        return this.titles;
    }

    /**
     * @return a pane that contains the right side of the header
     */
    private Pane right() {
        EventHandler<ActionEvent> btAction = new NavBarHandlerAction(this.m);
        this.gp = new ToggleGroup();
        // if the current user is logged in
        if (Objects.isNull(this.m.getCurrentUser())) {
            super.createButtons(VisitorHeaderContent.values(), btAction);
        } else {
            super.createButtons(UserHeaderContent.values(), btAction);
            if (this.m.getCurrentUser().isAdmin())
                super.createButtons(AdminHeaderContent.values(), btAction);
            HBox.setHgrow(this.navBox, Priority.ALWAYS);
            this.navBox.setSpacing(15.);
            this.navBox.setAlignment(Pos.BASELINE_RIGHT);
        }

        return this.navBox;
    }


    /**
     * refresh the current active pane
     */
    public void refreshCurrent() {
        super.refreshCurrent();
        if (this.m.isConnected())
            if (!this.getChildren().contains(this.m.getCurrentUser().getImg())) {
                UserAvatar img = this.m.getCurrentUser().getImg();
                // when we click on the user avatar we go back to home
                img.setOnMouseClicked(new SceneSwitchHandler(this.m, HeaderContent.Profile));
                img.getStyleClass().add("HeaderAvatar");
                this.getChildren().add(img);
            }
    }

    @Override
    public void refresh() {
        super.refresh();
        this.getChildren().clear();
        // refresh left and right too
        this.getChildren().addAll(this.left(), this.right());
        this.refreshCurrent();
    }

}
