package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.Handlers.FriendActionHandler;
import DuelSurLaToile.View.Handlers.FriendChangeListener;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.List;


public class FriendListPane extends VBox {
    private User[] friendList;
    private ListView<UserPane> listViewFriendList;
    private Main m;
    private ObservableList<UserPane> userList;

    public FriendListPane(Main m, SocialPane socialPane, List<User> friendList) {
        super();
        this.m = m;
//        this.prefHeightProperty().bind(Bindings.size(this.getItems()).multiply(this.getFixedCellSize()).add(30));
        this.prefHeightProperty().bind(this.m.getP().heightProperty().subtract(this.m.getHeaderPane().heightProperty()).subtract(this.m.getHeaderPane().heightProperty().getValue()));
        this.prefWidthProperty().bind(this.m.getP().widthProperty().divide(4).subtract(20));
//        this.setMinWidth(20);
        this.userList = FXCollections.observableArrayList();
        this.listViewFriendList = new ListView<>(this.userList);

        for (User u : friendList)
            this.userList.add(new UserPane(u));
        this.listViewFriendList.getSelectionModel().selectedItemProperty().addListener(new FriendChangeListener(socialPane));
        Label friendListLabel = new Label("Friend List :");
        friendListLabel.getStyleClass().add("socialLabel");
        VBox removeFriend = new VBox();
        removeFriend.getStyleClass().add("removeFriend");
        TextField tfRemoveFriend = new TextField();
        tfRemoveFriend.setPromptText("Pseudo");
        ToggleButton btRemoveFriend = new ToggleButton("Remove friend");
        btRemoveFriend.setOnAction(new FriendActionHandler(this.m, tfRemoveFriend));
        btRemoveFriend.getStyleClass().add("button");
        removeFriend.getChildren().addAll(tfRemoveFriend, btRemoveFriend);
        this.getChildren().addAll(friendListLabel, this.listViewFriendList, removeFriend);
    }

    public void refreshFriendsList(List<User> friendList) {
        this.listViewFriendList.getItems().clear();
        for (User u : friendList)
            this.userList.add(new UserPane(u));
    }

}
