package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.ContentInterface;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Should be the last of the HeaderContent nodes to be initialized in the main
 * THe body can only have 2 elements, a subNavbar and the body content
 */
public class BodyPane extends Pane {
    private final Main m;
    /**
     * String that represents the current location of the user
     */
    private String currentPaneName;

    public BodyPane(Main m) {
        super();
        this.m = m;
        this.getStyleClass().add("body");
        System.out.println(this.m.getHeaderPane());
//        this.prefWidthProperty().bind(this.m.getHeaderPane().widthProperty());
//        this.prefWidthProperty().bind(this.m.getScene().widthProperty());
    }public void setPrefWidthHeight(){
    }


    /**
     * set the current displayed content to the foreground
     *
     * @param toSwitch the value of the desired panel the users wants to access
     */
    public void setView(ContentInterface toSwitch) {
        this.currentPaneName = toSwitch.getValue();

        Method getMethod;
        try {
            getMethod = Main.class.getMethod(String.format("get%sPane", toSwitch.getValue()));
        } catch (NoSuchMethodException e) {
            // annoying because it normally won't happen
            System.out.println(String.format("Missing method %s in class Main", "get" + toSwitch.getValue() + "Pane"));
            e.printStackTrace();
            return;
        }
        Pane toSet;
        try {
            toSet = (Pane) getMethod.invoke(this.m);
        } catch (IllegalAccessException | InvocationTargetException e) {
            System.out.println(String.format("Missing parameters in %s in class Main", "get" + toSwitch.getValue() + "Pane"));
            e.printStackTrace();
            return;
        }

        this.setView(toSet);

    }

    /**
     * Allow object to be set in the body view
     *
     * @param objToSwitch have to be castable into an HeaderContentInterface
     */
    public void setView(Object objToSwitch) {
        ContentInterface toSwitch;
        try {
            toSwitch = (ContentInterface) objToSwitch;
        } catch (Exception e) {
            System.out.println("Failed to cast the given object into a contentInterface in the body class");
            e.printStackTrace();
            return;
        }
        this.setView(toSwitch);

    }


    /**
     * @param pane the desired pane to add
     */
    public void setView(Pane pane) {
        if (this.getChildren().contains(pane))
            return;
        if (this.getChildren().isEmpty())
            this.getChildren().add(pane);
        else
            this.getChildren().set(0, pane);
    }
    public Node getView(){
        if (this.getManagedChildren().size() <= 0 )
            return null;
        return this.getManagedChildren().get(0);
    }

    /**
     * @return the name of the current pane
     */
    public String getCurrentPaneName() {
        return this.currentPaneName;
    }

}
