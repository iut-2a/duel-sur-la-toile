package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.NavPaneWithSub;
import DuelSurLaToile.View.Handlers.GameListSelectorHandler;
import DuelSurLaToile.View.Handlers.GameStartHandler;
import DuelSurLaToile.View.Handlers.PlayActionHandler;
import DuelSurLaToile.View.Util.BoardContent;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.List;


/**
 * Class that contains the connect four and the chat
 * // todo Can handle many games
 */
public class PlayPane extends NavPaneWithSub {
    private final Main m;
    /**
     * the subNavBox which contains the BoarContent values
     */
    private final VBox navBox;
    private ObservableList<String> gameListToSee;
    private GamePane currentGamePane;
    private ComboBox<String> gameListSelector;
    private List<GamePane> gameList;
    private Button addNewGameButton;


    private Property<String> currentGameText;
    /**
     * contains the button to add new games and the combobox
     */
    private HBox listGameActive;
    private boolean isFirstGame;


    public PlayPane(Main m) {
        super(m);
        this.m = m;
        super.createButtons(BoardContent.values(), new PlayActionHandler(this.m));
        this.navBox = super.getNavBox();

        this.currentGamePane = new GamePane(this, false);

        this.isFirstGame = true;
        this.getChildren().add(this.navBox);
        this.createButtonList();
        HBox contents = new HBox(listGameActive, currentGamePane);


        this.getChildren().add(contents);


        // this.format();


    }

    public void createButtonList() {
        // contains the gameList and the add button
        System.out.println("initializing buttons");
        this.listGameActive = new HBox();
        this.gameList = FXCollections.observableArrayList();
        this.gameList.add(this.currentGamePane);

        this.gameListToSee = FXCollections.observableArrayList();
        this.gameListSelector = new ComboBox<String>(gameListToSee);
        this.gameListSelector.setMaxHeight(30);
        this.gameListSelector.setOnAction(new GameListSelectorHandler(this));
        this.currentGameText = new SimpleStringProperty();
        this.gameListSelector.valueProperty().bindBidirectional(currentGameText);


        this.addNewGameButton = new Button("+");
        this.addNewGameButton.setOnAction(new GameStartHandler(this));
        listGameActive.getChildren().addAll(this.gameListSelector, addNewGameButton);
        listGameActive.setSpacing(50);
        listGameActive.setVisible(false);
    }

    public GamePane getGamePane() {
        return this.currentGamePane;
    }

    /**
     * add a new game and make this one the current game
     */
    public void startNewGame() {
        // create buttons only when the first game is started
        listGameActive.setVisible(true);
        if (this.isFirstGame) {
            this.isFirstGame = false;
            this.gameListToSee.add("Game number " + (this.gameList.size()));
            this.currentGamePane.startNewGame(this.gameList.size());

        } else {
            this.currentGamePane = new GamePane(this);
            this.gameList.add(this.currentGamePane);
            this.gameListToSee.add("Game number " + (this.gameList.size()));

            this.setGame(this.gameList.size() - 1);

        }
        this.currentGameText.setValue("Game number " + (this.gameList.size()));

        System.out.println("GameList size: " + this.gameList.size());

    }

    /**
     * set the current game according to i
     *
     * @param i the game index to set
     */
    public void setGame(int i) {
        this.currentGameText.setValue("Game number " + (i + 1));
        if (i >= this.gameList.size()) {
            System.out.println("index out of bounds when tried to set the game");
            return;
        }
        GamePane toSet = this.gameList.get(i);
        this.currentGamePane = toSet;
        if (this.getChildren().contains(toSet))
            return;
        if (this.getChildren().size() < 3)
            this.getChildren().add(toSet);
        else
            this.getChildren().set(2, toSet);
    }

/*

    private void format() {
        this.width.addListener((listener, oldValue, newValue) -> {
            System.out.println("format, new Value : " + newValue.intValue());
            if (newValue.doubleValue() > 500)
                //this.connectFour.resize(newValue.intValue()/19);
                this.connectFourView.getChildren().clear();
            this.connectFourView = this.connectFour();
            this.add(this.connectFourView, 1, 1);

        });
    }
*/

}
