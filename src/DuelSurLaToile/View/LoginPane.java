
package DuelSurLaToile.View;

import DuelSurLaToile.Main;
import DuelSurLaToile.Models.LoginModel;
import DuelSurLaToile.View.Handlers.LoginActionHandler;
import DuelSurLaToile.View.Util.VisitorHeaderContent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class LoginPane extends VBox {
    private final Main m;
    private TextField username;
    private PasswordField password;
    private Button validate;
    private Button register;

    public LoginPane(Main m) {
        super();
        this.m = m;
        this.getStyleClass().add("LoginPane");
        this.initializeForms();

        this.setMinSize(800, 600);
//        this.setPadding(new Insets(15, 12, 15, 12));
//        this.setSpacing(10);
    }

    private void initializeForms() {
        this.createTitle();
        this.createUserForm();
        this.createPasswordForm();
        this.createButtonsForm();
    }

    /**
     * creates and add forms
     */

    private void createTitle(){
        HBox titrebox = new HBox();
        Label titre = new Label("Sign in");
        titre.setFont(new Font("Arial", 36));
        titrebox.getChildren().addAll(titre);
        titrebox.setAlignment(Pos.CENTER);
        this.getChildren().add(titrebox);

    }

    private void createUserForm() {
        HBox user = new HBox();
        this.username = new TextField();
        this.username.setPromptText("Username");
        this.username.setPrefSize(200,40);
        user.getChildren().addAll(this.username);
        user.setAlignment(Pos.CENTER);
        this.getChildren().add(user);

    }

    private void createPasswordForm() {
        HBox passwd = new HBox();
        this.password = new PasswordField();
        this.password.setPromptText("Password");
        this.password.setPrefSize(200,40);
        passwd.getChildren().addAll(this.password);
        this.getChildren().add(passwd);
    }


    /**
     * initialize the button form and add it to this
     */

    private void createButtonsForm() {
        HBox buttons = new HBox();
        LoginModel log = new LoginModel(this.m);
        var loginAction = new LoginActionHandler(this, log);
        this.validate = new Button(VisitorHeaderContent.Login.getValue());
        this.register = new Button(VisitorHeaderContent.Register.getValue());
        this.validate.setOnMouseClicked(loginAction);
        this.register.setOnMouseClicked(loginAction);

        buttons.getChildren().addAll(validate, register);
        this.getChildren().add(buttons);
    }

    public String getUsername() {
        return this.username.getText();
    }

    public String getPassword() {
        return this.password.getText();
    }

    public void setRegisterPane() {
        this.m.getBodyPane().setView(VisitorHeaderContent.Register);
        this.m.refreshHeader();
    }

}

