package DuelSurLaToile.View;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

/*
 * @deprecated
 */
public class FooterPane extends BorderPane {

    public FooterPane() {
        super();
        System.out.println("Footer created");
        Label legal = new Label("Legal notice 2020");
        legal.setTextFill(Color.BLACK);
        this.setLeft(legal);
        setMargin(legal, new Insets(0, 20, 0, 0));
        this.getStyleClass().add("footer");
    }
}