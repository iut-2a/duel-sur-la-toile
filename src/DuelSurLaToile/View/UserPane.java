package DuelSurLaToile.View;

import DuelSurLaToile.Models.User;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

/**
 * A pane that represents an user without his userAvatar
 */
public class UserPane extends Pane {
    /**
     * the represented user
     */
    private final User user;

    public UserPane(User user) {
        super();
        this.user = user;
        this.getChildren().addAll(new Label(this.user.getPseudo()));
    }

    public User getUser() {
        return this.user;
    }
}
