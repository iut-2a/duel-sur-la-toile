package DuelSurLaToile.Database;

import java.sql.*;

public class ConnectionSql {
    private Connection mysql;
    private boolean connecte = false;

    public ConnectionSql() throws ClassNotFoundException {
    }

    public void connecter(String nomServeur, String nomBase, String nomLogin, String motDePasse) throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Driver MySQL non trouvé?");
            try {
                Class.forName("org.mariadb.jdbc.Driver");
            } catch (ClassNotFoundException classNotFoundException) {
                System.out.println("Driver MariaDB non trouvé?");
            }
            mysql = null;
            return;
        }
        try {
            mysql = DriverManager.getConnection(
                    "jdbc:mysql://" + "46.105." +
                            "92.223" + ":3306/" + nomBase + "?useUnicode=yes&characterEncoding=UTF-8", nomLogin, motDePasse);
            connecte = true;
        } catch (SQLException e) {
            System.out.println("Echec de connexion!");
            System.out.println(e.getMessage());
            mysql = null;
        }
    }

    public void close() throws SQLException {
        this.mysql.close();
    }

    public boolean isConnecte() {
        return this.connecte;
    }

    public Blob createBlob() throws SQLException {
        return this.mysql.createBlob();
    }

    public Statement createStatement() throws SQLException {
        return this.mysql.createStatement();
    }

    public PreparedStatement prepareStatement(String requete) throws SQLException {
        return this.mysql.prepareStatement(requete);
    }
}