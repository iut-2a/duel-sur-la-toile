package DuelSurLaToile.Database;

import DuelSurLaToile.Models.Message;
import DuelSurLaToile.Models.User;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseMessage {

    /**
     * Have to be initialized before everything
     */
    private static ConnectionSql connection;

    public static void setConnection(ConnectionSql connection) {
        DatabaseMessage.connection = connection;
    }

    public static boolean addMessage(int idut_exp, int idut_dest, String contenu) {
        int i;
        try {
            PreparedStatement ps = DatabaseMessage.connection.prepareStatement(
                    "insert into MESSAGE(datemsg, contenumsg, idut_exp, idut_dest)\n" +
                            "        values (?, ?, ?, ?)"
            );

            ps.setDate(1, new Date(System.currentTimeMillis()));

            ps.setString(2, contenu);
            ps.setInt(3, idut_exp);
            ps.setInt(4, idut_dest);


            i = ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            i = 0;
        }
        return i != 0;
    }

    private static Message getMessageWithQuery(String query, Object toSet) throws SQLException {
        Message res = null;
        PreparedStatement ps = DatabaseMessage.connection.prepareStatement(query);
        try {
            ps.setObject(1, toSet);
            ps.setObject(2, toSet); // only if 2 parameters are required
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int idCheck = rs.getInt("idmsg");     // todo in test, check if idCheck == id
                Date datemsg = rs.getDate("datemsg");
                String contenumsg = rs.getString("contenumsg");
                int idut_exp = rs.getInt("idut_exp");
                int idut_dest = rs.getInt("idut_dest");
                res = new Message(idut_exp, idut_dest, contenumsg);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static List<Message> getAllMessage(User user1, User user2) {
        List<Message> res = new ArrayList<>();
        PreparedStatement ps = null;
        try {
            ps = DatabaseUser.connection.prepareStatement("select idmsg, datemsg, contenumsg, idut_exp, idut_dest from  MESSAGE where idut_exp=? and idut_dest = ? or idut_exp = ? and idut_dest = ? order by idmsg desc limit 5");
            ps.setObject(1, user1.getId());
            ps.setObject(2, user2.getId());
            ps.setObject(3, user2.getId());
            ps.setObject(4, user1.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int idCheck = rs.getInt("idmsg");     // todo in test, check if idCheck == id
                Date datemsg = rs.getDate("datemsg");
                String contenumsg = rs.getString("contenumsg");
                int idut_exp = rs.getInt("idut_exp");
                int idut_dest = rs.getInt("idut_dest");
                res.add(new Message(idut_exp, idut_dest, contenumsg));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

}

