package DuelSurLaToile.Database;


import DuelSurLaToile.Models.FriendRequest;
import DuelSurLaToile.Models.User;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static DuelSurLaToile.Database.DatabaseUser.getUserById;

//import org.jetbrains.annotations.Nullable;

/**
 * this class is a library
 */
public class DatabaseFriend {

    /**
     * Have to be initialized before everything
     */
    private static ConnectionSql connection;

    public static void setConnection(ConnectionSql connection) {
        DatabaseFriend.connection = connection;
    }

    public static boolean addFriend(User user1, User user2) throws SQLException {
        // todo

        PreparedStatement ps = DatabaseUser.connection.prepareStatement(
                "insert into ETREAMI(idut_1, idut_2)\n" +
                        "        values (?, ?)"
        );
        ps.setInt(1, user1.getId());
        ps.setInt(2, user2.getId());


        int i = ps.executeUpdate();
        return i != 0;
    }

    public static boolean removeFriend(User user1, User user2) throws SQLException {
        // todo

        PreparedStatement ps = DatabaseUser.connection.prepareStatement(
                "delete from ETREAMI where idut_1 = ? and idut_2 = ? or idut_1 =?  and idut_2 =  ? ");
        ps.setInt(1, user1.getId());
        ps.setInt(2, user2.getId());
        ps.setInt(3, user2.getId());
        ps.setInt(4, user1.getId());


        int i = ps.executeUpdate();
        return i != 0;
    }


    private static List<FriendRequest> getRequestWithQuery(String query, Object toSet) throws SQLException {
        List<FriendRequest> res = new ArrayList<>();
        PreparedStatement ps = DatabaseFriend.connection.prepareStatement(query);
        try {
            ps.setObject(1, toSet);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int idCheck = rs.getInt("idinv");     // todo in test, check if idCheck == id
                Date dateinv = rs.getDate("dateinv");
                String etatinv = rs.getString("etatinv");
                int idut_exp = rs.getInt("idut_exp");
                int idut_dest = rs.getInt("idut_dest");
                String msginv = rs.getString("msginv");
                res.add(new FriendRequest(getUserById(idut_exp), getUserById(idut_dest)));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Renvoi une liste des match du joueur
     *
     * @param friendRequest dont on veux les matchs joués
     * @return List<Match>
     */
    public static boolean addFriendInvitation(FriendRequest friendRequest) throws SQLException {
        // todo

        PreparedStatement ps = DatabaseUser.connection.prepareStatement(
                "insert into INVITATION(dateinv, etatinv, idut_exp, idut_dest, msginv)\n" +
                        "        values (?, ?, ?, ?, ?)"
        );
        ps.setDate(1, friendRequest.getDateinv());
        ps.setString(2, friendRequest.getEtatinv());
        ps.setInt(3, friendRequest.getUserexp().getId());
        ps.setInt(4, friendRequest.getUserdest().getId());
        ps.setString(5, friendRequest.getMessage());


        int i = ps.executeUpdate();
        return i != 0;
    }

    /**
     * Renvoi une liste de friend request pour l'utilisateur choisi
     *
     * @param user dont on veux les friend request
     * @return List<FriendRequest>
     */
    public static List<FriendRequest> getFriendRequestForUser(User user) {
        try {
            return DatabaseFriend.getRequestWithQuery("select idinv, dateinv, etatinv, idut_exp, idut_dest, msginv from  INVITATION where idut_dest=? and etatinv = 'Pending'", user.getId());
        } catch (SQLException e) {
            System.out.println("No friendRequest");
            return null;
        }
    }

    /**
     * Renvoi une liste des match du joueur
     *
     * @param idut_exp
     * @param idut_dest
     * @throws SQLException
     */
    public static void refuseFriendRequest(int idut_exp, int idut_dest) throws SQLException {
        try {
            PreparedStatement ps = DatabaseFriend.connection.prepareStatement(
                    "update INVITATION set etatinv = ? where idut_exp = ? and idut_dest = ?");

            ps.setString(1, "Rejected");
            ps.setInt(2, idut_exp);
            ps.setInt(3, idut_dest);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    /**
     * Renvoi une liste des match du joueur
     *
     * @param idut_exp
     * @param idut_dest
     * @throws SQLException
     */
    public static void acceptFriendRequest(int idut_exp, int idut_dest) throws SQLException {
        try {
            PreparedStatement ps = DatabaseFriend.connection.prepareStatement(
                    "update INVITATION set etatinv = ? where idut_exp = ? and idut_dest = ?");
            ps.setString(1, "Accepted");
            ps.setInt(2, idut_exp);
            ps.setInt(3, idut_dest);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    public static List<User> getFriendListByUser(User user) {
        List<User> res = new ArrayList<>();
        PreparedStatement ps = null;
        try {
            ps = DatabaseUser.connection.prepareStatement("select idut_1, idut_2 from ETREAMI where idut_1 = ? or idut_2 = ?");
            ps.setObject(1, user.getId());
            ps.setObject(2, user.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int idut_1 = rs.getInt("idut_1");
                int idut_2 = rs.getInt("idut_2");
                if (idut_1 == user.getId()) {
                    res.add(getUserById(idut_2));
                } else {
                    res.add(getUserById(idut_1));
                }
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
}
