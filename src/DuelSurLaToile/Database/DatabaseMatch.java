package DuelSurLaToile.Database;

import DuelSurLaToile.Models.Match;
import DuelSurLaToile.Models.User;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static DuelSurLaToile.Database.DatabaseUser.getUserById;

public class DatabaseMatch {
    private static ConnectionSql connection;

    public static void setConnection(ConnectionSql connection) {
        DatabaseMatch.connection = connection;
    }

    private static Match getMatchWithQuery(String query, Object toSet) throws SQLException {
        Match res;
        PreparedStatement ps = DatabaseMatch.connection.prepareStatement(query);
        ps.setObject(1, toSet);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            int idCheck = rs.getInt("idpa");     // todo in test, check if idCheck == id
            String debutpa = rs.getString("debutpa");
            int numetape = rs.getInt("numetape");
            String etatpartie = rs.getString("etatpartie");
            int idut_1 = rs.getInt("idut_1");
            int idut_2 = rs.getInt("idut_2");
            int score_1 = rs.getInt("score_1");
            int score_2 = rs.getInt("score_2");
            if (idut_2 == 0)   // no player 2
                res = new Match(idCheck, getUserById(idut_1), etatpartie);
            else
                res = new Match(idCheck, getUserById(idut_1), getUserById(idut_2), etatpartie);
        } else {
            throw new SQLException("Match non trouvé");
        }
        return res; // todo
    }

    /**
     * Ajoute un Match à la base de données
     *
     * @param match
     * @return boolean
     */
    public static boolean addMatch(Match match) throws SQLException {
        PreparedStatement ps = DatabaseMatch.connection.prepareStatement(
                "insert into PARTIE(debutPa, numEtape, etatPartie, idut_1, idut_2, score_1, score_2)\n" +
                        "        values (?, ?, ?, ?, ?, ?, ?)"
        );
        ps.setDate(1, match.getDateStart());
        ps.setInt(2, match.getNumRound());
        ps.setString(3, match.getStatePa());
        ps.setInt(4, match.getUser1().getId());
        ps.setInt(5, match.getUser2().getId());
        ps.setInt(6, match.getScoreUser1());
        ps.setInt(7, match.getScoreUser2());

        int i = ps.executeUpdate();
        return i != 0;
    }

    public static void updateMatch(Match match) throws SQLException {
        int idut_2 = match.getUser2().getId();
        int idpa = match.getId();
        String etat = match.getStatePa();

        int numRound = match.getNumRound();
        int score1 = match.getScoreUser1();
        int score2 = match.getScoreUser2();
        try {
            PreparedStatement ps = DatabaseMatch.connection.prepareStatement(
                    "update PARTIE set idut_2= ?, etatpartie = ?, numetape = ?, score_1 = ?, score_2 = ? where idpa = ?");

            ps.setInt(1, idut_2);
            ps.setString(2, etat);
            ps.setInt(3, numRound);
            ps.setInt(4, score1);
            ps.setInt(5, score2);
            ps.setInt(6, idpa);

            ps.executeUpdate();
        } catch (SQLException se) {
            se.printStackTrace();
            return;
        }
    }

    /**
     * @return an id for a new Match
     */
    public static int getNewId() {
        // todo
        return (int) (Math.random() * 452345);
    }

    /**
     * Renvoi un Match qui est en attend d'un joueur
     *
     * @return Match
     */
    public static Match getMatchWaitingP() {
        try {
            return DatabaseMatch.getMatchWithQuery("select idpa, debutpa, numetape, etatpartie, idut_1, idut_2, score_1, score_2 from  PARTIE where etatpartie =?", "Waiting for P");
        } catch (SQLException e) {
            System.out.println("No waiting match");
            return null;
        }
    }

    /**
     * Renvoi une liste des match du joueur
     *
     * @param user dont on veut les matchs joués
     * @return List<Match>
     */
    public static List<Match> getMatchByUser(User user) {
        try {
            return DatabaseMatch.getListMatchWithQuery("select idpa, debutpa, numetape, etatpartie, idut_1, idut_2, score_1, score_2 from  PARTIE where idut_1=? or idut_2=? order by idpa desc limit 5", user.getId());
        } catch (SQLException e) {
            System.out.println("No waiting match");
            return null;
        }
    }

    private static List<Match> getListMatchWithQuery(String query, Object toSet) throws SQLException {
        List<Match> res = new ArrayList<>();
        PreparedStatement ps = DatabaseMatch.connection.prepareStatement(query);
        try {
            ps.setObject(1, toSet);
            ps.setObject(2, toSet); // only if 2 parameters are required
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int idCheck = rs.getInt("idpa");     // todo in test, check if idCheck == id
                String debutpa = rs.getString("debutpa");
                int numetape = rs.getInt("numetape");
                String etatpartie = rs.getString("etatpartie");
                int idut_1 = rs.getInt("idut_1");
                int idut_2 = rs.getInt("idut_2");
                int score_1 = rs.getInt("score_1");
                int score_2 = rs.getInt("score_2");
                res.add(new Match(idCheck, getUserById(idut_1), getUserById(idut_2), etatpartie, score_1, score_2, numetape));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res; // todo
    }

    public static Match getMatchById(int idpa) {
        try {
            return DatabaseMatch.getMatchWithQuery("select idpa, debutpa, numetape, etatpartie, idut_1, idut_2, score_1, score_2 from  PARTIE where idpa=?", idpa);
        } catch (SQLException e) {
            System.out.println("No matching match with selected Id");
            return null;
        }
    }

    public static boolean addWaiting(Match match) throws SQLException {
        PreparedStatement ps = DatabaseMatch.connection.prepareStatement(
                "insert into PARTIE(debutPa, numEtape, etatPartie, idut_1, score_1, score_2)\n" +
                        "        values (?, ?, ?, ?, ?, ?)"
        );
        ps.setDate(1, match.getDateStart());
        ps.setInt(2, match.getNumRound());
        ps.setString(3, match.getStatePa());
        ps.setInt(4, match.getUser1().getId());
        ps.setInt(5, match.getScoreUser1());
        ps.setInt(6, match.getScoreUser2());

        int i = ps.executeUpdate();
        return i != 0;
    }

    public static Match getIdMatchWithDate(Date dateStart) {
        try {
            return DatabaseMatch.getMatchWithQuery("select idpa, debutpa, numetape, etatpartie, idut_1, idut_2, score_1, score_2 from  PARTIE where debutpa=?", dateStart);
        } catch (SQLException e) {
            System.out.println("No matching match with selected date");
            e.printStackTrace();
            return null;
        }
    }
}
