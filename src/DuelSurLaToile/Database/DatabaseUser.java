package DuelSurLaToile.Database;

import DuelSurLaToile.Models.*;
import javafx.scene.image.Image;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

//import org.jetbrains.annotations.Nullable;

/**
 * this class is a library
 */
public class DatabaseUser {

    public static ConnectionSql connection;

    public static void setConnection(ConnectionSql connection) {
        DatabaseUser.connection = connection;
    }

    private static int getIntWithQuery(String query, Object toSet) {
        int res;
        try {
            PreparedStatement ps = DatabaseUser.connection.prepareStatement(query);
            ps.setObject(1, toSet);
            ps.setObject(2, toSet);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                res = rs.getInt("itemCount");     // todo in test, check if idCheck == id
            } else
                res = 0;
        } catch (SQLException e) {
            e.printStackTrace();
            res = 0;
        }
        return res; // todo
    }

    private static User getUserWithQuery(String query, Object toSet) throws SQLException {
        User res;
        PreparedStatement ps = DatabaseUser.connection.prepareStatement(query);
        ps.setObject(1, toSet);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            int idCheck = rs.getInt("idut");     // todo in test, check if idCheck == id
            String pseudo = rs.getString("pseudout");
            String email = rs.getString("emailut");
            String mdp = rs.getString("mdput");
            String mdpSalt = rs.getString("mdpSalt");
            boolean disabled = rs.getString("activeut").equals("a");
            String role = rs.getString("nomrole");
            if (role.equals("admin"))
                res = new Admin(idCheck, pseudo, email, new Password(mdp, mdpSalt), disabled);
            else
                res = new User(idCheck, pseudo, email, new Password(mdp, mdpSalt), disabled);
//            System.out.println("" + toSet + " " + pseudo + " " + email);

        } else {
            throw new SQLException("Joueur non trouvé");
        }
        return res; // todo
    }

//    public static @Nullable
//    User getUserById(int id) throws SQLException {
//        return DataBaseUser.getUserWithQuery("select idut, pseudout, emailut, mdput, mdpSalt, activeut, nomrole from  UTILISATEUR where idut =?", id);
//    }

    /**
     * Renvoi User associé au pseudo voulu
     *
     * @param username dont on veut le User
     * @return User
     */
    public static User getUserByUserName(String username) {
        try {
            return DatabaseUser.getUserWithQuery("select idut, pseudout, emailut, mdput, mdpSalt, activeut, nomrole from  UTILISATEUR where pseudout =?", username);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("failed to retrieve the user: " + username);
            return null; // :(
        }
    }

    /**
     * Renvoi User associé à l'id voulu
     *
     * @param userId dont on veux le User
     * @return User
     */
    public static User getUserById(int userId) {
        try {
            return DatabaseUser.getUserWithQuery("select idut, pseudout, emailut, mdput, mdpSalt, activeut, nomrole from  UTILISATEUR where idut =?", userId);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("failed to retrieve the user: " + userId);
            return null; // :(
        }
    }

    public static byte[] getImageBytes(User user) {
        try {
            PreparedStatement ps = DatabaseUser.connection.prepareStatement(
                    "select imageut from UTILISATEUR where pseudout =?"
            );
            ps.setString(1, user.getPseudo());
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                Blob blob = result.getBlob("imageut");
                if (Objects.isNull(blob))
                    return null;
                byte[] imgBytes = blob.getBytes(1, (int) blob.length());
                if (imgBytes.length == 0)
                    return null;

                System.out.println("Profile picture retrieved");
                return imgBytes;
            } else {
                System.out.println("Failed to retrieve user img");
                return null; // sorry
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Ajoute un User à la base de données
     *
     * @param user
     * @return boolean
     */
    public static boolean insertUser(User user) throws SQLException {
        // todo

        PreparedStatement ps = DatabaseUser.connection.prepareStatement(
                "insert into UTILISATEUR(pseudout, emailut, mdput, mdpSalt, activeut, nomrole)\n" +
                        "        values (?, ?, ?, ?, ?, ?,?)"
        );
        ps.setString(1, user.getPseudo());
        ps.setString(2, user.getEmail());
        ps.setString(3, user.getMdp().getHashedPwd());
        ps.setString(4, user.getMdp().getSalt());
        ps.setString(5, "a");
        ps.setString(6, "user");


        System.out.println(user.getMdp());
        int i = ps.executeUpdate();
        return i != 0;
    }

    public static void setImage(int userId, File image) {
        PreparedStatement ps = null;
        try {
            ps = DatabaseUser.connection.prepareStatement(
                    "UPDATE UTILISATEUR\n" +
                            "SET imageut =?\n" +
                            "WHERE idut=?");
            FileInputStream fis = new FileInputStream(image);
            ps.setBlob(1, fis, (int) (image.length()));
            ps.setInt(2, userId);
            ps.executeUpdate();
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

    }

    /**
     * @return the number of win of the given user
     */
    public static int getWinCount(User user) {
        return DatabaseUser.getIntWithQuery("SELECT COUNT(idPa) as itemCount FROM PARTIE where idut_1=? and score_1=1 or idut_2=? and score_2=1", user.getId());
    }

    /**
     * @return the number of lose of the given user
     */
    public static int getLoseCount(User user) {
        return DatabaseUser.getIntWithQuery("SELECT COUNT(idPa) as itemCount FROM PARTIE where idut_1=? and score_1=0 or idut_2=? and score_2=0", user.getId());
    }

    /**
     * @return the number of lose of the given user
     */
    public static int getNbGame(User user) {
        int res = DatabaseUser.getIntWithQuery("SELECT COUNT(idPa) as itemCount FROM PARTIE where idut_1=? or idut_2=?", user.getId());
        if (res == 0) {
            return 1;
        }
        return res;
    }

    /**
     * @param currentUser
     * @param userToSet
     * @return the msg list beetween two users
     */
    public static List<Message> getMessages(User currentUser, User userToSet) {
        return DatabaseMessage.getAllMessage(currentUser, userToSet);
        // return new MessagePane[]{new MessagePane("salut", u1, true), new MessagePane("j'aime la vie", u2, false)};
    }

    public static User[] getAllUsers() {
        List<User> res = new ArrayList<>();
        PreparedStatement ps = null;
        try {
            ps = DatabaseUser.connection.prepareStatement("select idut,\n" +
                    "       pseudout,\n" +
                    "       emailut,\n" +
                    "       mdput,\n" +
                    "       mdpSalt,\n" +
                    "       activeut,\n" +
                    "       imageut,\n" +
                    "       nomrole\n" +
                    "from UTILISATEUR; ");


            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int idCheck = rs.getInt("idut");     // todo in test, check if idCheck == id
                String pseudo = rs.getString("pseudout");
                String email = rs.getString("emailut");
                String mdp = rs.getString("mdput");
                String mdpSalt = rs.getString("mdpSalt");
                boolean disabled = rs.getString("activeut").equals("a");
                String role = rs.getString("nomrole");
                res.add(new User(idCheck, pseudo, email, new Password(mdp, mdpSalt), disabled));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        User[] userArray = new User[res.size()];
        userArray = res.toArray(userArray);

        return userArray;
    }


    /**
     * @param imgBytes a mySql img is stored as a byte array
     */
    public void setImage(byte[] imgBytes) {
        Image img = new Image(new ByteArrayInputStream(imgBytes));
        // todo
    }

    public boolean isActivated(int id) {
        // todo
        return true;
    }

    public Match[] getCurrentMatches(int id) {
        // todo
        return null;
    }

    public Match[] getAllMatches(int id) {
        // todo
        return null;
    }

    public Match[] getMatchWith(int playerId) {
        // todo
        return null;
    }

    public static User[] getUserInvitation(User u) {
        // todo
        return new User[]{};
    }

    public static void insertMessage(User user, User dest, String msg) {

        // insert message content
        PreparedStatement ps = null;
        try {
            ps = DatabaseUser.connection.prepareStatement(
                    "        insert into MESSAGE(datemsg, contenumsg, idut) values (?,?,?)"
            );
            ps.setDate(1, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            ps.setString(2, msg);
            ps.setInt(3, user.getId());

            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        int lastMsgId = DatabaseUser.getLastIdMsg(user);

        // add msg content to dest
        try {
            ps = DatabaseUser.connection.prepareStatement(
                    "insert into RECEVOIR(idmsg, idut, lu) values (?,?,?)"
            );
            ps.setInt(1, lastMsgId);
            ps.setInt(2, dest.getId());
            ps.setString(3, "n");
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static int getLastIdMsg(User user) {
        PreparedStatement ps = null;    // todo res not equals 0
        int res;
        try {
            ps = DatabaseUser.connection.prepareStatement(
                    "        select idmsg from MESSAGE where idut =?"
            );
            ps.setInt(1, user.getId());

            ResultSet rs = ps.executeQuery();
            if (rs.next())
                res = rs.getInt("idmsg");
            else
                res = 0;
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            res = 0;
        }
        return res;
    }

    public static void desactivateUser(String username) {
        try {
            PreparedStatement ps = DatabaseUser.connection.prepareStatement(
                    "update UTILISATEUR set activeut=? where pseudout = ?");

            ps.setString(1, "d");
            ps.setString(2, username);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    public static void activateUser(String username) {
        try {
            PreparedStatement ps = DatabaseUser.connection.prepareStatement(
                    "update UTILISATEUR set activeut=? where pseudout = ?");

            ps.setString(1, "a");
            ps.setString(2, username);

            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

}

