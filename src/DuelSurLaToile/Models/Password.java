package DuelSurLaToile.Models;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Objects;

/**
 * Many thanks to Lokesh Gupta with his tutorial on password hashing
 * https://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
 */

/**
 * Higly securised password
 * default salt is the username
 */
public class Password {
    /**
     * hashdPwd.split(':')
     * at 0 : number of iterations
     * at 1: salt
     * at 2: hashedpwd
     */
    private String hashedPwd;
    /**
     * represents the number of iterations we wants on the salt
     * the bigger the better but will take more time
     */
    private static final int ITERATIONS = 1000;

    public Password(String pwd) {
        try {
            this.hashedPwd = this.generateStrongPasswordHash(pwd);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            System.out.println("failed to generate password");
            e.printStackTrace();
        }
    }

    public Password(String hashedPwd, String salt) {
        this.hashedPwd = "" + ITERATIONS + ":" + salt + ":" + hashedPwd;
    }

    private static byte[] generateSalt() {
        SecureRandom sr = null;
        try {
            sr = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    public String getHashedPwd() {
        return this.hashedPwd.split(":")[2];
    }

    /**
     * Compares the bytes arrays in length-constant time.
     * This comparison method is used to avoid password hashes to be extracted
     * on a timing attack on-line and off-line.
     *
     * @param hash     the first byte array
     * @param testHash the second byte array to compare
     * @return boolean that indicates if these byte arrays are equals
     */
    private static boolean slowEquals(byte[] hash, byte[] testHash) {
        int diff = hash.length ^ testHash.length;
        for (int i = 0; i < hash.length && i < testHash.length; i++) {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }

    /**
     * @param array the byte array
     * @return a String that represents this byte array
     */
    private static String toHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    /**
     * @param password to hash
     * @return a hashed string of this password
     */
    private String generateStrongPasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        int iterations = ITERATIONS;
        char[] chars = password.toCharArray();
        byte[] salt = generateSalt();

        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
    }

    /**
     * @param toTest the password to test
     * @return
     */
    public boolean validatePassword(String toTest) {
        String[] parts = this.hashedPwd.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);

        PBEKeySpec spec = new PBEKeySpec(toTest.toCharArray(), salt, iterations, hash.length * 8);
        SecretKeyFactory skf;
        try {
            skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("PBKDF2WithHmacSHA1 algorithm wasn't found for passwordHashing");
            e.printStackTrace();
            return false;
        }
        byte[] testHash;
        try {
            testHash = skf.generateSecret(spec).getEncoded();
        } catch (InvalidKeySpecException e) {
            System.out.println("unable to generate the secret key for USER password");
            e.printStackTrace();
            return false;
        }
        return Password.slowEquals(hash, testHash);
    }

    /**
     * @param hex an hashed password string
     * @return the corresponding bytes array
     */
    private byte[] fromHex(String hex) {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

    public String getSalt() {
        return this.hashedPwd.split(":")[1];
    }

    public String toString() {
        return "salt : " + this.getSalt() + " hashedPwd: " + this.getHashedPwd();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Password)) return false;

        Password password = (Password) o;

        return Objects.equals(hashedPwd, password.hashedPwd);
    }

    @Override
    public int hashCode() {
        return hashedPwd != null ? hashedPwd.hashCode() : 0;
    }
}
