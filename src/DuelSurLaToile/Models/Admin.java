package DuelSurLaToile.Models;

public class Admin extends User {

    public Admin(int id, String pseudo, String email, String mdp, boolean isActive) {
        super(id, pseudo, email, mdp, isActive);
    }

    public Admin(int idCheck, String pseudo, String email, Password password, boolean disabled) {
        super(idCheck, pseudo, email, password, disabled);
    }

    @Override
    public boolean isAdmin() {
        return true;
    }
}
