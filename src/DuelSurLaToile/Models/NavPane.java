package DuelSurLaToile.Models;

import DuelSurLaToile.Main;
import javafx.event.EventHandler;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class NavPane extends HBox {
    private final HBox navBox;
    private final ToggleGroup gp;
    private final Main m;
    /**
     * Contains every buttons in the header
     */
    private List<ToggleButton> btList;

    protected NavPane(Main m) {
        this.m = m;
        this.gp = new ToggleGroup();
        this.navBox = new HBox();
        this.btList = new ArrayList<>();
    }

    public Main getMain() {
        return this.m;
    }

    protected void createButtons(ContentInterface[] enu, EventHandler btAction) {

        for (ContentInterface h : enu) {
            ToggleButton bt = new ToggleButton(h.getValue());
            this.btList.add(bt);
            this.navBox.getChildren().add(bt);
            bt.getStyleClass().add("headerBt");
            bt.setToggleGroup(gp);
            bt.setOnAction(btAction);
        }
    }

    protected HBox getNavBox() {
        return this.navBox;
    }

    protected ToggleGroup getToggleGroup() {
        return this.gp;
    }

    protected ToggleButton getButtonByName(String btName) {
        for (ToggleButton bt : this.btList)
            if (bt.getText().equals(btName))
                return bt;
        return null; // btName wasn't found
    }

    /**
     * Unselect every buttons except the passed one
     *
     * @param bt the button to set active
     */
    protected void setSelected(ToggleButton bt) {
        for (ToggleButton button : this.btList)
            button.setSelected(button.equals(bt));
    }


    /**
     * refresh the navBox
     */
    public void refresh() {
        this.navBox.getChildren().clear();
        this.btList = new ArrayList<>();
    }

    /**
     * refresh the current active pane
     */
    public void refreshCurrent() {
        ToggleButton bt = this.getButtonByName(this.m.getBodyPane().getCurrentPaneName());
        if (Objects.isNull(bt)) {
            System.out.println("Header wasn't able to refresh the current button");
            return;
        }
        this.setSelected(bt);
    }
}

