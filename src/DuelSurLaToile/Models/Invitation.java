package DuelSurLaToile.Models;

import DuelSurLaToile.Database.DatabaseUser;


public class Invitation extends Message {

    public Invitation(int idut_exp, int idut_dest) {
        super(idut_exp, idut_dest, DatabaseUser.getUserById(idut_exp).getPseudo() + "  sent you an invitation");
    }

    @Override
    public boolean isInvit() {
        return true;
    }
}
