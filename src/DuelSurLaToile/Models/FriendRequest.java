package DuelSurLaToile.Models;


import DuelSurLaToile.Database.DatabaseFriend;

import java.sql.Date;
import java.util.List;

public class FriendRequest {
    private final Date dateinv;
    private final User userexp;
    private final User userdest;
    private final String message;
    private final String etatinv;

    public FriendRequest(User userexp, User userdest){
        this.dateinv = new java.sql.Date(System.currentTimeMillis());
        this.userexp = userexp;
        this.userdest = userdest;
        this.message = "ok";
        this.etatinv = "Pending";
    }

    public Date getDateinv() {
        return this.dateinv;
    }

    public String getEtatinv() {
        return this.etatinv;
    }

    public String getMessage() {
        return this.message;
    }

    public User getUserexp() {
        return this.userexp;
    }

    public User getUserdest() {
        return this.userdest;
    }

    public List<FriendRequest> getFriendRequestForUser(User user){
        return DatabaseFriend.getFriendRequestForUser(user);
    }
}
