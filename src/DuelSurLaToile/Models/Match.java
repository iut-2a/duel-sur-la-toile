package DuelSurLaToile.Models;

import DuelSurLaToile.Database.DatabaseMatch;

import java.sql.Date;
import java.sql.SQLException;
import java.util.Objects;

/**
 * @implNote cette classe même si elle parrait peut utile simmplifiera bcp le travail plus tard ;)
 */
public class Match {
    // a match is with 2 players
    private final User u1;
    private User u2;
    private final int score1;
    private final int score2;
    private final Date dateStart;
    private final int numRound;
    /**
     * waiting for p when game is not started
     * +u1 when just started
     * +(last user who makes the move) colIndex
     */
    private String statePa;
    private int id;

    public static String WAITING = "waiting for p";

    // todo

    public Match(User user1, String waiting) {
        this(user1, null, waiting);
    }

    public Match(User user1, User user2, String waiting) {
        this.u1 = user1;
        this.u2 = user2;
        this.id = 0;
        this.score1 = 3;
        this.score2 = 3;
        this.statePa = waiting;
        this.numRound = 0;
        this.dateStart = new Date(System.currentTimeMillis());
    }

    public Match(int id, User user1, String waiting) {
        this(user1, waiting);
        this.id = id;
    }

    public Match(int id, User user1, User user2, String waiting) {
        this.id = id;
        this.u1 = user1;
        this.u2 = user2;
        this.score1 = 3;
        this.score2 = 3;
        this.statePa = waiting;
        this.numRound = 0;
        this.dateStart = new Date(System.currentTimeMillis());
    }

    public Match(int id, User user1, User user2, String waiting, int score1, int score2, int numRound) {
        this.id = id;
        this.u1 = user1;
        this.u2 = user2;
        this.score1 = score1;
        this.score2 = score2;
        this.statePa = waiting;
        this.numRound = numRound;
        this.dateStart = new Date(System.currentTimeMillis());
    }

    public User getUser1() {
        return this.u1;
    }

    public User getUser2() {
        return this.u2;
    }

    public int getScoreByUser(User u) {
        if (u.equals(this.u1))
            return this.score1;
        return this.score2;
    }

    public int getScoreUser1() {
        return this.score1;
    }

    public int getScoreUser2() {
        return this.score2;
    }

    public Date getDateStart() {
        return this.dateStart;
    }

    public int getNumRound() {
        return this.numRound;
    }

    public int getId() {
        return this.id;
    }

    public String getStatePa() {
        return this.statePa;
    }

    /**
     * set the player 2 to the given player
     *
     * @param user the user to add
     */
    public void addUser(User user) {
        if (Objects.nonNull(this.u2)) {
            System.out.println("Error, user 2 was already defined");
            return;
        }
        this.u2 = user;
        this.statePa = "+ u1";
        try {
            DatabaseMatch.updateMatch(this);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * update the match id, because when  created this one is null
     */
    public void update() {
        Match match;
        if (Objects.nonNull(match = DatabaseMatch.getIdMatchWithDate(this.dateStart))) {
            this.id = match.id;
            this.statePa = match.statePa;
        }
    }

    /**
     * update the match with the given state
     *
     * @param newState the new state to set
     */
    public void updateState(String newState) {
        Match match;
        this.statePa = newState;
        try {
            DatabaseMatch.updateMatch(this);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Match)) return false;

        Match match = (Match) o;

        if (score1 != match.score1) return false;
        if (score2 != match.score2) return false;
        if (numRound != match.numRound) return false;
        if (id != match.id) return false;
        if (!u1.equals(match.u1)) return false;
        if (!Objects.equals(u2, match.u2)) return false;
        if (!Objects.equals(dateStart, match.dateStart)) return false;
        return Objects.equals(statePa, match.statePa);
    }

    @Override
    public int hashCode() {
        int result = u1.hashCode();
        result = 31 * result + (u2 != null ? u2.hashCode() : 0);
        result = 31 * result + score1;
        result = 31 * result + score2;
        result = 31 * result + (dateStart != null ? dateStart.hashCode() : 0);
        result = 31 * result + numRound;
        result = 31 * result + (statePa != null ? statePa.hashCode() : 0);
        result = 31 * result + id;
        return result;
    }
}
