package DuelSurLaToile.Models;

/**
 * @implNote cette classe même si elle parrait peut utile simmplifiera bcp le travail plus tard ;)
 */

import DuelSurLaToile.Database.DatabaseFriend;
import DuelSurLaToile.Database.DatabaseMatch;
import DuelSurLaToile.Database.DatabaseUser;
import DuelSurLaToile.View.Util.UserAvatar;
import javafx.scene.image.Image;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class User {
    public static final Image DEFAULTAVATAR = new Image("/resources/images/avatar.png");
    //** ATTRIBUTS **//
    /**
     *
     */
    private String pseudo;
    /**
     *
     */
    private int id;
    /**
     *
     */
    private ArrayList<User> listeAmis;
    /**
     *
     */
    private Date signUpDate, bornDate;
    /**
     *
     */
    private boolean isActive;
    /**
     *
     */
    private String email;
    /**
     *
     */
    private Password mdp;

    private byte[] imgBytes;


    //** CONSTRUCTEURS **//

    /**
     * Contructeur avec seulement id
     *
     * @param id
     */
    public User(int id) {
        this.id = id;
    }

    /**
     * @param id
     * @param pseudo
     * @param email
     * @param mdp
     * @param isActive
     */
    public User(int id, String pseudo, String email, String mdp, boolean isActive) {

        this.pseudo = pseudo;
        this.email = email;
        this.mdp = new Password(mdp); // when we don't have salt
        this.isActive = isActive;
        this.id = id;
    }


    /**
     * create an active user with an id set to 0
     *
     * @param pseudo
     * @param email
     * @param pwd
     * @param isActive
     */
    public User(String pseudo, String email, Password pwd, boolean isActive) {
        this(0, pseudo, email, pwd, isActive);
    }

    public User(String pseudo, String email, Password mdp) {
        this(0, pseudo, email, mdp, true);
    }

    public User(int idCheck, String pseudo, String email, Password pwd, boolean disabled) {
        this.pseudo = pseudo;
        this.email = email;
        this.mdp = pwd;
        this.isActive = disabled;
        this.id = idCheck;
    }


    //** METHODE **//

    /**
     * Renvoi le pseudo du joueur
     *
     * @return
     */
    public String getPseudo() {
        return this.pseudo;
    }

    /**
     * Renvoi l'ID du joueur
     *
     * @param
     * @return
     */
    public int getId() {
        return this.id;
    }

    public Password getMdp() {
        return this.mdp;
    }

    public String getEmail() {
        return this.email;
    }


    public static boolean addFriend(User user1, User user2) throws SQLException {
        return DatabaseFriend.addFriend(user1, user2);
    }

    public static void acceptFriendRequest(int idut_exp, int idut_dest) throws SQLException {
        DatabaseFriend.acceptFriendRequest(idut_exp, idut_dest);
    }

    public int getWinCount() {
        return DatabaseUser.getWinCount(this);
    }

    /**
     * Renvoie le ratio victoires/défaites du Joueur
     *
     * @return
     */
    public double getWinRate() {
        return (double) this.getWinCount() / this.getNbGame() * 100;
    }

    /**
     * Setter de la date de naissance du Joueur
     */
    public void setBornDate(Date bornDate) {
        this.bornDate = bornDate;
    }

    /**
     * Setter de la date d'inscription du Joueur
     */
    public void setSignUpDate() {
        this.signUpDate = new Date();
    }

    /**
     * Setter de l'id du Joueur
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Renvoie la liste des parties terminées du joueur
     *
     * @return liste des parties terminées du joueur
     */
    public List<Match> getMatchHistory() {
        return DatabaseMatch.getMatchByUser(this);
    }

    /**
     * Ajoute un match à l'historique du joueur
     *
     * @param match le match à ajouter
     */
    public void addMatchHistory(Match match) throws SQLException {
        DatabaseMatch.addMatch(match);
    }

    public boolean isAdmin() {
        return false;
    }

    public int getLoseCount() {
        return DatabaseUser.getLoseCount(this);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (isActive != user.isActive) return false;
        if (!Objects.equals(pseudo, user.pseudo)) return false;
        if (!Objects.equals(email, user.email)) return false;
        return Objects.equals(mdp, user.mdp);
    }

    @Override
    public int hashCode() {
        int result = pseudo != null ? pseudo.hashCode() : 0;
        result = 31 * result + id;
        result = 31 * result + (isActive ? 1 : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (mdp != null ? mdp.hashCode() : 0);
        return result;
    }

    public List<User> getFriendList() {
        // todo
        return DatabaseFriend.getFriendListByUser(this);
    }

    public User[] getInvitationGameList() {
        // todo
        return new User[]{new User("paprika", "paprika@lesCochons.fr", new Password("roseMelon"), true)};
    }

    @Override
    public String toString() {
        return "User : " + this.pseudo;
    }

    public int getNbGame() {
        return DatabaseUser.getNbGame(this);
    }

    public UserAvatar getImg() {
        if (Objects.isNull(this.imgBytes)) {
            byte[] userImg = DatabaseUser.getImageBytes(this);
            if (Objects.nonNull(userImg)) {
                this.imgBytes = userImg;
                return new UserAvatar(userImg);
            } else    // better to create a new one each time
                return new UserAvatar();
        } else
            return new UserAvatar(this.imgBytes);

    }

    public static void refuseFriendRequest(int idut_exp, int idut_dest) throws SQLException {
        DatabaseFriend.refuseFriendRequest(idut_exp, idut_dest);
    }

    public boolean isActive() {
        return this.isActive;
    }

    public List<FriendRequest> getFriendRequestForUser(User user) {
        return DatabaseFriend.getFriendRequestForUser(user);
    }
}