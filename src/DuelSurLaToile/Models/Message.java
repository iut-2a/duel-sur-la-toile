package DuelSurLaToile.Models;

public class Message {
    private final int idut_exp;
    private final int idut_dest;
    private final String contenumsg;
    private boolean isInvit;

    public Message(int idut_exp, int idut_dest, String contenumsg) {
        this.idut_exp = idut_exp;
        this.idut_dest = idut_dest;
        this.contenumsg = contenumsg;
    }

    public int getIdut_dest() {
        return this.idut_dest;
    }

    public int getIdut_exp() {
        return this.idut_exp;
    }

    public String getContenumsg() {
        return this.contenumsg;
    }

    public boolean isInvit() {
        return false;
    }
}
