package DuelSurLaToile.Models;

import DuelSurLaToile.Main;
import javafx.event.EventHandler;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Pane wich contains subNavBars
 */
public abstract class NavPaneWithSub extends VBox {
    private final Main m;
    private final VBox navBox;
    private final ToggleGroup gp;
    private final HBox nav;
    /**
     * Contains every buttons in the header
     */
    private final List<ToggleButton> btList;

    protected NavPaneWithSub(Main m) {
        this.m = m;
        this.gp = new ToggleGroup();
        this.navBox = new VBox();
        this.nav = new HBox();
        this.navBox.getChildren().add(this.nav);
        this.nav.getStyleClass().add("SubNavBox");
        this.btList = new ArrayList<>();
    }

    protected VBox getNavBox() {
        return this.navBox;
    }

    protected ToggleGroup getToggleGroup() {
        return this.gp;
    }

    /**
     * @return the main class instance
     */
    public Main getMain() {
        return this.m;
    }

    /**
     * @param enu      an enum values that implements ContentInterface
     * @param btAction the action to set on every added buttons
     */
    protected void createButtons(ContentInterface[] enu, EventHandler btAction) {
        int i = 0;
        for (ContentInterface h : enu) {
            ToggleButton bt = new ToggleButton(h.getValue());
            this.btList.add(bt);
            this.nav.getChildren().add(bt);
            bt.getStyleClass().add("headerBt");
            bt.setToggleGroup(gp);
            bt.setOnAction(btAction);
            if (i == 0)
                bt.setSelected(true);
            ++i;
        }
    }

    /**
     * set the current view of profile to the given panel
     *
     * @param enumType a value of the profileContent enum
     */
    public static Pane getViewToSet(Main m, ContentInterface enumType) {
        String value = enumType.getValue();
        Method getMethod;
        try {
            getMethod = m.getClass().getMethod("get" + value + "Pane");
        } catch (NoSuchMethodException e) {
            System.out.println(String.format("Method get%sPane in Main class is missing", value));
            e.printStackTrace();
            return null;
        }

        Pane toSet;
        try {
            toSet = (Pane) getMethod.invoke(m);
        } catch (IllegalAccessException | InvocationTargetException e) {
            System.out.println("method get" + value + "Pane in Main class probably have parameters");
            e.printStackTrace();
            return null;
        }
        return toSet;
    }

    /**
     * @param toSet the desired pane to add
     */
    public void setView(Pane toSet) {
        if (this.getChildren().contains(toSet))
            return;
        if (this.getChildren().size() < 2)
            this.getChildren().add(toSet);
        else
            this.getChildren().set(1, toSet);
        this.refreshCurrent();
    }

    /**
     * @param btName String of the desired button
     * @return the Button in which the text is the btName
     */
    protected ToggleButton getButtonByName(String btName) {
        for (ToggleButton bt : this.btList)
            if (bt.getText().equals(btName))
                return bt;
        return null; // btName wasn't found
    }

    /**
     * refresh the current selected button
     */
    public void refreshCurrent() {
        // todo
    }

    /**
     * set every buttons unselected except of the button passed in parameter
     *
     * @param bt the button to set selected
     */
    protected void setSelected(ToggleButton bt) {
        for (ToggleButton button : this.btList)
            button.setSelected(button.equals(bt));
    }

}
