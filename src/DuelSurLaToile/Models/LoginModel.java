package DuelSurLaToile.Models;

import DuelSurLaToile.Database.DatabaseUser;
import DuelSurLaToile.Main;
import DuelSurLaToile.View.Util.HeaderContent;

import java.sql.SQLException;
import java.util.Objects;

/**
 * Used for visitors when they want to log in or registering
 */
public class LoginModel {
    private final Main m;

    public LoginModel(Main m) {
        this.m = m;
    }

    /**
     * @param username
     * @param pwd
     * @return boolean that indicates if the given username and pwd matches in the database
     */
    public boolean attemptConnection(String username, String pwd) {
        // TODO
        User u = DatabaseUser.getUserByUserName(username);
        if (Objects.isNull(u))
            return false;
        return u.getMdp().validatePassword(pwd);
    }

    /**
     * Set the user to the current user and redirect him to the home panel
     *
     * @param u the new user to become the current user
     */
    public void setUser(User u) {
        if (Objects.isNull(u))
            return;
        this.m.setCurrentUser(u);
        this.m.getBodyPane().setView(HeaderContent.Home);
        this.m.majAffichage();
    }

    /**
     * attempt to register a new user, if he succeeded, he set the current user to this one
     *
     * @param username
     * @param email
     * @param pwd
     * @return boolean that indicates if the registration is successful
     */
    public boolean attemptRegistration(String username, String email, String pwd) {
        Password pass = new Password(pwd);
        User newUser = new User(username, email, pass);
        // int id = DataBaseUser.getIdByName(username) // todo
        try {
            DatabaseUser.insertUser(newUser);
        } catch (SQLException throwable) {
            System.out.println("error during user insertion");
            return false;       //todo user cannot be inserted
        }
        this.setUser(newUser);
        return true;
    }
}
