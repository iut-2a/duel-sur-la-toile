package DuelSurLaToile;

import DuelSurLaToile.Database.*;
import DuelSurLaToile.Games.Handlers.Dev;
import DuelSurLaToile.Models.User;
import DuelSurLaToile.View.*;
import DuelSurLaToile.View.Util.VisitorHeaderContent;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.sql.SQLException;
import java.util.Objects;

/**
 * Main view
 * Divided in two parts:
 * - the header
 * - the body
 */
public class Main extends Application {
    /**
     * main Scene
     */
    private Scene root;

    private BorderPane p;

    /**
     * Body models that handle body views
     */
    private BodyPane body;

    /**
     * Header models that handle header the header views
     */
    private HeaderPane headerPane;

    /**
     * One of the body's views
     * represents the profile of the user
     */
    private ProfilePane profile;
    /**
     * One of the body's views
     * represents the profile of the user looking for
     */
    private ProfilePane otherUserProfile;

    /**
     * One of the body's views
     * represents the play panel
     */
    private PlayPane play;

    /**
     * One of the body's views
     * represents the homepage
     */
    private HomePane homePane;

    /**
     * One of the body's views
     * represents login screen
     * Can be see only if the current user isn't logged
     */
    private LoginPane loginPane;

    /**
     * One of the body's views
     * represents register screen
     * Can be see only if the current user isn't logged
     */
    private RegisterPane registerPane;

    /**
     * One of the body's views
     * represents the administration panel when the user is an admin
     */
    private AdminPanel adminPanel;

    /**
     * allow to connect to our database
     */
    private ConnectionSql connectionSql;

    /**
     * represents the current user
     * if this one is not initialized, we are considered as a visitor
     */
    private User currentUser;
    private Stage stage;


    @Override
    public void init() throws ClassNotFoundException, SQLException {
        // we have to first initialize the database
        this.initializeSql();
        this.loginPane = new LoginPane(this);
        this.registerPane = new RegisterPane(this);
        this.p = new BorderPane();
        this.play = new PlayPane(this);
        // play is by default on the game panel
//        this.play.setView(NavPaneWithSub.getViewToSet(this, BoardContent.Game));
        this.homePane = new HomePane(this);
        this.body = new BodyPane(this);
        this.headerPane = new HeaderPane(this);
        this.otherUserProfile = new ProfilePane(this);
        this.profile = new ProfilePane(this);
        this.adminPanel = new AdminPanel(this);
        this.body.setView(VisitorHeaderContent.Login);

        // default profile view
        this.profile.setView(this.profile.getOverviewPane());
    }


    /**
     * initialize the Database
     *
     * @throws ClassNotFoundException if the java sql driver wasn't found
     */
    private void initializeSql() throws ClassNotFoundException, SQLException {
        try {
            this.connectionSql = new ConnectionSql();
        } catch (ClassNotFoundException e) {
            System.out.println("You don't have java sql connector, sorry");
            throw new ClassNotFoundException();
        }
        this.connectionSql.connecter("46.105.92.223",
                "db22a", "groupe22a",
                "20@info!iuto22a");
        DatabaseUser.setConnection(this.connectionSql);
        DatabaseMatch.setConnection(this.connectionSql);
        DatabaseFriend.setConnection(this.connectionSql);
        DatabaseMessage.setConnection(this.connectionSql);
    }

    private BodyPane getCenter() {
        // no need to format
        return this.body;
    }

    private Pane getTop() {
        return this.headerPane;
    }

    private Pane getBottom() {
        return null;    // no footer
        //return new Footer();
    }

    public ProfilePane getProfilePane() {
        return this.profile;
    }

    public BodyPane getBodyPane() {
        return this.body;
    }

    public PlayPane getPlayPane() {
        return this.play;
    }

    /**
     * useful with our implementations in the playPane when we click on the subButton game
     *
     * @return the desired Pane
     */
    public GamePane getGamePane() {
        return this.play.getGamePane();
    }

    public HeaderPane getHeaderPane() {
        return this.headerPane;
    }

    public BorderPane getP() {
        return this.p;
    }

    public HomePane getHomePane() {
        return this.homePane;
    }


    public AdminPanel getAdminPane() {
        return this.adminPanel;
    }

    /**
     * useful with our implementations in the profilePane when we click on the subButton social
     *
     * @return
     */
    public SocialPane getSocialPane() {
        return this.profile.getSocialPane();
    }

    /**
     * useful with our implementations in the profilePane when we click on the subButton history
     *
     * @return
     */
    public HistoryPane getHistoryPane() {
        return this.profile.getHistoryPane();
    }

    /**
     * useful with our implementations in the profilePane when we click on the subButton overview
     *
     * @return
     */
    public OverviewPane getOverviewPane() {
        return this.profile.getOverviewPane();
    }


    public User getCurrentUser() {
        return this.currentUser;
    }

    public ProfilePane getOtherUserProfile() {
        return otherUserProfile;
    }

    public void setOtherUserProfile(ProfilePane otherUserProfile) {
        this.otherUserProfile = otherUserProfile;
    }

    public void setCurrentUser(User user) {
        System.out.println("new user: " + user.getPseudo());
        this.currentUser = user;
    }

    public LoginPane getLoginPane() {
        return this.loginPane;
    }

    public RegisterPane getRegisterPane() {
        return this.registerPane;
    }

    /**
     * met à jour l'affichage
     */
    public void majAffichage() {
        //this.headerPane.refresh();
        this.homePane.refresh();
        this.profile.refresh();
        this.refreshHeader();
        this.refreshSubHeader();
        this.getSocialPane().initializeSocial();
        this.getHistoryPane().createHistoryView();
        this.adminPanel.refresh();
    }

    /**
     * refresh only the header
     */
    public void refreshHeader() {
        this.headerPane.refresh();
    }

    /**
     * refresh the subButtons of the header
     */
    public void refreshSubHeader() {
        /*
        this.play.refreshCurrent();
        this.profile.refreshCurrent();
        */
        // todo, it would allow to click on the button of the current pane et let him selected
        // luxury
    }

    public void refreshOverview() {
//        this.getOverviewPane().setUser(currentUser);
        this.getOverviewPane().refresh();
    }

    public void refreshOtherUserOverview() {
        this.getOtherUserProfile().getOverviewPane().refresh();
    }

    public void refreshMatchHistory() {
        this.getHistoryPane().setUser(currentUser);
        this.getHistoryPane().refresh();
    }

    public void refreshOtherUserMatchHistory() {
        this.getOtherUserProfile().getHistoryPane().refresh();
    }

    /**
     * @return the main scene
     */
    public Scene getScene() {
        return this.root;
    }

    public Stage getStage() {
        return this.stage;
    }

    @Override
    public void start(Stage stage) throws Exception {

        this.stage = stage;
        stage.setTitle("Duel sur la toile");
        Scene root = new Scene(p, 850, 850);
        this.root = root;
        root.getStylesheets().add("resources/css/stylesheet.css");

        this.p.setTop(this.getTop());
        this.p.setBottom(this.getBottom());


        this.refreshHeader();


        this.p.setCenter(this.getCenter());
        if (Dev.BOARDEBBUGING)
            System.out.println("Play.Board created");
        this.loginPane.prefWidthProperty().bind(this.p.widthProperty());
//        this.body.prefWidthProperty().bind(this.headerPane.widthProperty());

        stage.setScene(root);
        stage.show();
    }

    /**
     * @return true if the user is logged in, else false
     */
    public boolean isConnected() {
        return Objects.nonNull(this.currentUser);
    }

    public void logout() {
        this.currentUser = null;
    }
}
